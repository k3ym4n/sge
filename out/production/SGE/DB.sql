CREATE DATABASE if not EXISTS micropresupuestos;

use micropresupuestos;

create table if not EXISTS usuarios(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR (50) not null,
apellido VARCHAR (100) not null,
direccion VARCHAR (100) not null,
email VARCHAR (100)  not null,
fecha_registro DATETIME,
telefono_fijo VARCHAR (25),
telefono_movil VARCHAR (25) not null,
tipo ENUM ('ADMINISTRADOR','OPERARIO','CONSULTA')  not null,
nick varchar (50) not null,
password VARCHAR (50) not null
);

INSERT INTO `usuarios` (`id`, `nombre`, `apellido`, `direccion`, `email`, `fecha_registro`, `telefono_fijo`, `telefono_movil`, `tipo`, `nick`, `password`)
VALUES ('1', 'xxxxxxxxxxxx', 'xxxxxxxxxxxxxxxxxxxx', 'xxxxxxxxxxxxxxxx', 'email@email.email', '1978-04-19 15:33:15', 'xxxxxxxxxxxx', 'xxxxxxxxxxx',
						 'ADMINISTRADOR', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997');

create table if not EXISTS clientes(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR (100)  not null,
cif VARCHAR (10),
dni VARCHAR (10),
direccion VARCHAR (150) not null,
cp VARCHAR (50) not null,
provincia VARCHAR (35)  not null,
email VARCHAR (150)  not null,
telefono_contacto VARCHAR (50)  not null,
telefono2_contacto VARCHAR (50),
codigo VARCHAR (75),
tipo ENUM ('PARTICULAR','EMPRESA'),
id_usuario INT UNSIGNED NOT NULL,
	INDEX (id_usuario),
	FOREIGN KEY (id_usuario) REFERENCES usuarios (id)
);

create table if not EXISTS pedidos(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
codigo VARCHAR(25),
nombre VARCHAR (250),
referencia VARCHAR (250),
fecha_inicio DATE  not null,
fecha_fin DATE,
estado ENUM ('PENDIENTE','PROCESANDO','FINALIZADO') not null,
importe FLOAT UNSIGNED NOT NULL,
observaciones varchar (250),
id_cliente INT UNSIGNED NOT NULL,
	INDEX (id_cliente),
	FOREIGN KEY (id_cliente) REFERENCES clientes (id)
);

create table if not EXISTS presupuestos(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
fecha DATE not null,
importe FLOAT UNSIGNED not null,
numpresupuesto int unsigned,
estado ENUM ('PENDIENTE','CONFIRMADO','ANULADO') not null,
observaciones varchar (250),

id_pedido INT UNSIGNED NOT NULL,
	INDEX (id_pedido),
	FOREIGN KEY (id_pedido) REFERENCES pedidos(id)

);

create table if not EXISTS productos(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
referencia varchar(25),
codigo VARCHAR (12),
denominacion VARCHAR(250),
tintas enum ('SINTINTAS','BN1','BN2','COLOR1BN1','COLOR1','COLOR2'),
importe FLOAT unsigned not null,
observaciones varchar (250)
);


create table if not EXISTS pedidos_productos(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
unidades INT unsigned,
precio FLOAT unsigned,


id_pedido INT UNSIGNED NOT NULL,
	INDEX (id_pedido),
	FOREIGN KEY (id_pedido) REFERENCES pedidos(id)
	ON DELETE CASCADE ON UPDATE NO ACTION,

id_producto INT UNSIGNED NOT NULL,
	INDEX (id_producto),
	FOREIGN KEY (id_producto) REFERENCES productos(id)
	ON DELETE CASCADE ON UPDATE NO ACTION
);


create table if not EXISTS procesos(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
codigo varchar (30),
nombre varchar (50),
pvp_minimo float unsigned,
pvp_hora float unsigned,
observaciones varchar (250)
);

create table if not EXISTS procesos_productos(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
importe float,
tiempo float,

id_procesos INT UNSIGNED NOT NULL,
	INDEX (id_procesos),
	FOREIGN KEY (id_procesos) REFERENCES procesos(id)
		ON DELETE CASCADE ON UPDATE NO ACTION,

id_producto INT UNSIGNED NOT NULL,
	INDEX (id_producto),
	FOREIGN KEY (id_producto) REFERENCES productos(id)
		ON DELETE CASCADE ON UPDATE NO ACTION

);