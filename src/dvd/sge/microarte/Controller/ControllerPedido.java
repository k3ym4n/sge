package dvd.sge.microarte.Controller;

import dvd.sge.microarte.base.Cliente;
import dvd.sge.microarte.base.DetallePedido;
import dvd.sge.microarte.base.Pedido;
import dvd.sge.microarte.base.Producto;
import dvd.sge.microarte.face.Botonera;
import dvd.sge.microarte.face.vDetallePedido;
import dvd.sge.microarte.face.vPedido;
import dvd.sge.microarte.util.Conexion;
import dvd.sge.microarte.util.EstadoPedido;
import dvd.sge.microarte.util.TipoUsuario;
import dvd.sge.microarte.util.Util;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ControllerPedido implements ActionListener {

    private JFrame frame;
    private vPedido vPedido;
    private Model model;
    private Botonera botonera;

    private DefaultTableModel dtmPedido,dtmCliente,dtmDetallePedido;
    private Pedido pedido;
    private Cliente cliente;

    private vDetallePedido vDetallePedido;
    private ArrayList<DetallePedido> detallePedidos;

    private int cantidad;
    private Producto producto;
    private DetallePedido detallePedido;

    private String nickusuario = ControllerLogin.login.tfnick.getText();




    public ControllerPedido(vPedido vPedido, Model model, Botonera botonera) {
        this.vPedido = vPedido;
        this.model = model;
        this.botonera = botonera;

        vPedido.lbnick.setText(nickusuario);
        nivelAccesoUsuario(model.comprobarUsuario(nickusuario).getTipoUsuario());

        detallePedidos = new ArrayList<>();

        listenerBotonera();
        comboBoxEstadoPedido();
        modeloTablaPedido();
        modeloTablaCliente();
        modeloTablaDetallePedido();

        listarTablaPedido();
        listarTablaCliente();

        listenerTablaPedido();
        listenerTablaCliente();
        listenerTablaDetalle();

        buscarNombre();
        buscarCodigo();
        buscarReferencia();
        buscarCliente();

        buscarEstadoPedido();

        botonera.btmodificar.setEnabled(false);
        botonera.bteliminar.setEnabled(false);
        vPedido.btaddDetallePedido.setEnabled(false);
        vPedido.bteliminarDetallePedido.setEnabled(false);
    }

    private void nivelAccesoUsuario(TipoUsuario tipoUsuario) {

        switch (tipoUsuario) {
            case CONSULTA:
                vPedido.menuBar.setEnabled(false);
                vPedido.panelBotonera.setVisible(false);
                vPedido.bteliminarDetallePedido.setVisible(false);
                vPedido.btaddDetallePedido.setVisible(false);
                vPedido.btrefrescarTablaClientes.setEnabled(false);
                break;
        }
    }


    private void listenerBotonera(){
        botonera.btguardar.addActionListener(this);
        botonera.btmodificar.addActionListener(this);
        botonera.btcancelar.addActionListener(this);
        botonera.bteliminar.addActionListener(this);
        vPedido.btrefrescarTablaClientes.addActionListener(this);
        vPedido.btprocesando.addActionListener(this);
        vPedido.btaddDetallePedido.addActionListener(this);

        vPedido.bteliminarDetallePedido.addActionListener(this);
        vPedido.btfechas.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource()==botonera.btguardar){
            if(comprobarCamposObligatorios() == true && guardarCampoCodigo() == true && comprobarCamposGuadar() == true){
                guardarPedido();
                comboBoxEstadoPedido();
                listarTablaPedido();
                limpiarCampos();
            }

        }else if(e.getSource()==botonera.btmodificar){
            if(comprobarCamposObligatorios() == true) {
                int filaPedido = vPedido.tablaPedido.getSelectedRow();
                String codigo = (String) vPedido.tablaPedido.getValueAt(filaPedido,0);
                pedido = model.getPedido(codigo);
                modificarPedido(pedido);
                listarTablaPedido();
                limpiarCampos();
                modeloTablaDetallePedido();
                detallePedidos.clear();
                botonera.btmodificar.setEnabled(false);
                botonera.bteliminar.setEnabled(false);
                vPedido.btaddDetallePedido.setEnabled(false);
                vPedido.bteliminarDetallePedido.setEnabled(false);
            }

        }else if(e.getSource()==botonera.bteliminar){

                int filaPedido = vPedido.tablaPedido.getSelectedRow();
                String codigo = (String) vPedido.tablaPedido.getValueAt(filaPedido, 0);
                pedido = model.getPedido(codigo);
                model.eliminarHibernate(pedido);
                listarTablaPedido();
                limpiarCampos();
                botonera.btmodificar.setEnabled(false);
                botonera.bteliminar.setEnabled(false);
                botonera.btguardar.setEnabled(true);


        }else if(e.getSource()==botonera.btcancelar){
            limpiarCampos();
            comboBoxEstadoPedido();
            modeloTablaDetallePedido();
            vPedido.btaddDetallePedido.setEnabled(false);

            vPedido.bteliminarDetallePedido.setEnabled(false);
            botonera.btmodificar.setEnabled(false);
            botonera.bteliminar.setEnabled(false);
            botonera.btguardar.setEnabled(true);
            detallePedidos.clear();

        }else if(e.getSource()==vPedido.btrefrescarTablaClientes){
            listarTablaCliente();

        }else if(e.getSource() == vPedido.btprocesando){

            if(comprobarCamposObligatorios() == true) {
                vPedido.cbEstadoPedido.setSelectedIndex(2);
                int filaPedido = vPedido.tablaPedido.getSelectedRow();
                String codigo = (String) vPedido.tablaPedido.getValueAt(filaPedido,0);

                pedido = model.getPedido(codigo);

                modificarPedido(pedido);
                comboBoxEstadoPedido();
                listarTablaPedido();
                limpiarCampos();
            }
        }else if(e.getSource() == vPedido.btaddDetallePedido){


            vDetallePedido = new vDetallePedido();
            vDetallePedido.lbcodigoPedido.setText(vPedido.tfCodigo.getText());
            vDetallePedido.visible();

            int cantidad = vDetallePedido.getCantidad();
            Producto producto = vDetallePedido.getProducto();
            Pedido pedido = vDetallePedido.getPedido();

            DetallePedido detallePedido = new DetallePedido();


            try{
                detallePedido.setUnidades(cantidad);
                detallePedido.setPrecio(cantidad * producto.getImporte());
                detallePedido.setPedido(pedido);
                detallePedido.setProducto(producto);
                float precio =  (cantidad * producto.getImporte());

                if(vPedido.tfimporte.getText().isEmpty()){
                    vPedido.tfimporte.setText(String.valueOf(precio));
                }else{

                    vPedido.tfimporte.setText(String.valueOf(Float.parseFloat(vPedido.tfimporte.getText()) + precio));
                }

                anadirDetallePedido(detallePedido);
                listarTablaAnadirDetallePedido();

            }catch(NullPointerException npe){
                vPedido.lbinfo.setText("Has cerrado la ventana de productos sin elegir producto");
            }

        }else if(e.getSource() == vPedido.bteliminarDetallePedido){



            try{
                int filaDetallePedido = vPedido.tablaDetallePedido.getSelectedRow();
                int id = (int) vPedido.tablaDetallePedido.getValueAt(filaDetallePedido,0);
                detallePedido = model.getDetallePedidoEliminar(id);
            }catch(ArrayIndexOutOfBoundsException abe){
                vPedido.lbinfo.setText("Tienes que escoger una fila del la tabla Detalles");
            }





            int unidades = detallePedido.getUnidades();
            float precio = detallePedido.getPrecio();
            detallePedido.getPedido();
            detallePedido.getProducto();


            vPedido.tfimporte.setText(String.valueOf(Float.parseFloat(vPedido.tfimporte.getText()) - precio  ));

            model.eliminarHibernate(detallePedido);

            listarTablaDetallePedidoCliente(detallePedido.getPedido().getId());



        }else if(e.getSource() == vPedido.btfechas){

            if(vPedido.datebuscarinicio.getDate() != null && vPedido.datebuscarFin.getDate() != null){
                buscarEntreFechas();
                vPedido.datebuscarinicio.setDate(null);
                vPedido.datebuscarFin.setDate(null);


            }else if(vPedido.datebuscarinicio.getDate() != null && vPedido.datebuscarFin.getDate() == null){
                buscarDesdeFecha();
                vPedido.datebuscarinicio.setDate(null);

            }else if(vPedido.datebuscarFin.getDate() != null && vPedido.datebuscarinicio.getDate() == null){
                vPedido.lbinfo.setText("No se puede realizar una consulta sin una fecha inicial de busqueda");
                vPedido.datebuscarFin.setDate(null);
            }else{
                vPedido.lbinfo.setText("Nesesitas al menos un campo fecha");
            }

        }


    }




    private void limpiarCampos(){
        vPedido.tfCodigo.setText("");
        vPedido.tfreferencia.setText("");
        vPedido.tfnombre.setText("");
        vPedido.Dateinicio.setDate(null);
        vPedido.Datefin.setDate(null);
        vPedido.tfimporte.setText("");
        vPedido.cbEstadoPedido.setSelectedItem("ESTADO PEDIDO");
        vPedido.taobservaciones.setText("");

        vPedido.lbcodigoCliente.setText("");
        vPedido.lbnombreCliente.setText("");
        vPedido.lbinfo.setText("");


    }

    private void modeloTablaPedido(){
        dtmPedido = new DefaultTableModel();
        dtmPedido.addColumn("Codigo");
        dtmPedido.addColumn("Referencia");
        dtmPedido.addColumn("Nombre");
        dtmPedido.addColumn("Fecha Inicio");
        dtmPedido.addColumn("Fecha Fin");
        dtmPedido.addColumn("Importe");
        dtmPedido.addColumn("Estado");
        dtmPedido.addColumn("Observaciones");
        dtmPedido.addColumn("Cliente");
        vPedido.tablaPedido.setModel(dtmPedido);

    }

    private  void modeloTablaCliente(){
        dtmCliente = new DefaultTableModel();
        dtmCliente.addColumn("Codigo");
        dtmCliente.addColumn("Nombre");
        dtmCliente.addColumn("D.N.I");
        dtmCliente.addColumn("C.I.F");
        vPedido.tablaClientes.setModel(dtmCliente);
    }

    private void modeloTablaDetallePedido(){
        dtmDetallePedido = new DefaultTableModel();
        dtmDetallePedido.addColumn("ID");
        dtmDetallePedido.addColumn("Nombre");
        dtmDetallePedido.addColumn("Unidades");
        dtmDetallePedido.addColumn("Precio/u");
        dtmDetallePedido.addColumn("SubTotal");
        vPedido.tablaDetallePedido.setModel(dtmDetallePedido);
    }

    private boolean comprobarCamposObligatorios(){

        Date date = new Date();
        Date fechainicio = vPedido.Dateinicio.getDate();
        if(vPedido.tfreferencia.getText().isEmpty()){
            JOptionPane.showMessageDialog(null,"El campo Referencia esta vacio");
            return  false;

        }else if(vPedido.tfnombre.getText().isEmpty()){
            JOptionPane.showMessageDialog(null,"El campo Nombre esta vacio");
            return  false;
        }else if(fechainicio == null || fechainicio.getDate() < date.getDate()){
            JOptionPane.showMessageDialog(null,"El campo Fecha Inicio esta vacio o es menor que la fecha actual");
            return false;

        }else if(vPedido.Datefin.getDate() == null){
            JOptionPane.showMessageDialog(null,"El campo Fecha Fin esta vacio");
            return false;
        }else if(vPedido.cbEstadoPedido.getSelectedIndex() <= 0){
            JOptionPane.showMessageDialog(null,"El campo Estado Pedido esta vacio");
            return false;
        }else if(vPedido.tfimporte.getText().isEmpty() || Util.esNumero(vPedido.tfimporte.getText()) == false){
            JOptionPane.showMessageDialog(null,"El campo Importe esta vacio");
            return false;
        }else if(vPedido.taobservaciones.getText().isEmpty()){
            vPedido.taobservaciones.setText("Sin Observaciones");
            return true;

        }

        return true;
    }

    private boolean comprobarCamposGuadar(){
        if(vPedido.lbcodigoCliente.getText().isEmpty()){
            JOptionPane.showMessageDialog(null,"Tienes que escoger un cliente de la tabla Clientes");
            return false;
        }else{
            return true;
        }
    }

    private void listarTablaPedido(){
        List<Pedido> listaPedidos = model.getListaPedidos();
        dtmPedido.setRowCount(0);
        for(Pedido pedido : listaPedidos){
            Object [] filaPedido = new Object[]{pedido.getCodigo(),pedido.getReferencia(),pedido.getNombre(),pedido.getFecha_inicio(),pedido.getFecha_fin(),
                    pedido.getImporte(),pedido.getEstadoPedido(),pedido.getObservaciones(),pedido.getCliente().getNombre()};
            dtmPedido.addRow(filaPedido);
        }

    }

    private void listarTablaCliente(){
        List<Cliente> listaClientes = model.getListaClientes();
        dtmCliente.setRowCount(0);
        for(Cliente cliente : listaClientes){
            Object [] filaCliente = new Object[]{cliente.getCodigo(),cliente.getNombre(),cliente.getDni(),cliente.getCif()};
            dtmCliente.addRow(filaCliente);

        }
    }

    private ArrayList<DetallePedido> listarDestalles(){

        return detallePedidos;

    }

    public void anadirDetallePedido(DetallePedido detalle) {

        detallePedidos.add(detalle);
    }

    private void listarTablaAnadirDetallePedido(){
        List<DetallePedido> listaDetalles = listarDestalles();
        dtmDetallePedido.setRowCount(0);
        for(DetallePedido detallePedido : listaDetalles){
            Object [] filadetalle = new Object[]{detallePedido.getId(),detallePedido.getProducto().getDenominacion(),detallePedido.getUnidades(),
                    detallePedido.getProducto().getImporte(),detallePedido.getPrecio()};
            dtmDetallePedido.addRow(filadetalle);
        }
    }





    private void listarTablaDetallePedidoCliente(int pedido){

        List<DetallePedido> listaDetalles = model.getDetallePedido(pedido);
        dtmDetallePedido.setRowCount(0);
        for(DetallePedido detallePedido : listaDetalles){
            Object [] filadetalle = new Object[]{detallePedido.getId(),detallePedido.getProducto().getDenominacion(),detallePedido.getUnidades(),
                    detallePedido.getProducto().getImporte(),detallePedido.getPrecio()};
            dtmDetallePedido.addRow(filadetalle);
        }
    }




    //TODO cuando se click sobre un pedido que en la tabla cliente se muestre de quien es


    private void listenerTablaPedido(){
        vPedido.tablaPedido.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                try{
                    Pedido pedido = model.getPedido((String) vPedido.tablaPedido.getValueAt(vPedido.tablaPedido.getSelectedRow(),0));
                    vPedido.tfCodigo.setText(pedido.getCodigo());
                    vPedido.tfreferencia.setText(pedido.getReferencia());
                    vPedido.tfnombre.setText(pedido.getNombre());
                    vPedido.Dateinicio.setDate(pedido.getFecha_inicio());
                    vPedido.Datefin.setDate(pedido.getFecha_fin());
                    vPedido.tfimporte.setText(String.valueOf(pedido.getImporte()));
                    vPedido.cbEstadoPedido.setSelectedItem(pedido.getEstadoPedido());
                    vPedido.taobservaciones.setText(pedido.getObservaciones());
                    vPedido.lbcodigoCliente.setText(pedido.getCliente().getCodigo());
                    vPedido.lbnombreCliente.setText(pedido.getCliente().getNombre());
                    listarTablaDetallePedidoCliente(pedido.getId());

                    botonera.btguardar.setEnabled(false);
                    botonera.btmodificar.setEnabled(true);
                    botonera.bteliminar.setEnabled(true);
                    vPedido.btaddDetallePedido.setEnabled(true);


                }catch(ArrayIndexOutOfBoundsException aiofbe){
                    vPedido.lbinfo.setText("Deberia darle otra vez a la fila del pedido que deses");
                }
            }
        });


    }

    private void listenerTablaCliente(){
        vPedido.tablaClientes.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Cliente cliente = model.comprobarClienteCodigo((String) vPedido.tablaClientes.getValueAt(vPedido.tablaClientes.getSelectedRow(),0));
                vPedido.lbcodigoCliente.setText(cliente.getCodigo());
                vPedido.lbnombreCliente.setText(cliente.getNombre());
            }
        });

    }

    private void listenerTablaDetalle(){
        vPedido.tablaDetallePedido.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vPedido.bteliminarDetallePedido.setEnabled(true);
            }
        });
    }



    private void comboBoxEstadoPedido(){
        vPedido.cbEstadoPedido.removeAllItems();
        vPedido.cbbuscarEstado.removeAllItems();
        vPedido.cbEstadoPedido.addItem("ESTADO PEDIDO");
        vPedido.cbbuscarEstado.addItem("ESTADO PEDIDO");
        EstadoPedido[] estadoPedidos = EstadoPedido.values();
        for (EstadoPedido estadoPedido : estadoPedidos) {
            vPedido.cbEstadoPedido.addItem(estadoPedido);
            vPedido.cbbuscarEstado.addItem(estadoPedido);
        }
    }

    private boolean guardarCampoCodigo(){

        if(model.getPedido(vPedido.tfCodigo.getText()) != null || vPedido.tfCodigo.getText().isEmpty()){
            JOptionPane.showConfirmDialog(null,"El campo Codigo esta vacio o ya existe");
            return  false;
        }

        return true;
    }

    private void guardarPedido(){
        pedido = new Pedido();
        pedido.setCodigo(vPedido.tfCodigo.getText());
        pedido.setReferencia(vPedido.tfreferencia.getText());
        pedido.setNombre(vPedido.tfnombre.getText());
        pedido.setFecha_inicio(vPedido.Dateinicio.getDate());
        pedido.setFecha_fin(vPedido.Datefin.getDate());
        pedido.setImporte(Float.parseFloat(vPedido.tfimporte.getText()));
        pedido.setObservaciones(vPedido.taobservaciones.getText());
        pedido.setEstadoPedido((EstadoPedido) vPedido.cbEstadoPedido.getSelectedItem());
        pedido.setCliente(model.comprobarClienteCodigo(vPedido.lbcodigoCliente.getText()));

        model.guardarHibernate(pedido);

    }


    private void modificarPedido(Pedido pedido){

        if(vPedido.tablaDetallePedido.getRowCount()!= 0){
            if(model.getPedido(vPedido.tfCodigo.getText()) != null){
                JOptionPane.showMessageDialog(null,"El codigo ya existe , no habra cambios en este campo");
                String codigo = pedido.getCodigo();
                pedido.setCodigo(codigo);
                pedido.setReferencia(vPedido.tfreferencia.getText());
                pedido.setNombre(vPedido.tfnombre.getText());
                pedido.setFecha_inicio(vPedido.Dateinicio.getDate());
                pedido.setFecha_fin(vPedido.Datefin.getDate());
                pedido.setImporte(Float.parseFloat(vPedido.tfimporte.getText()));
                pedido.setObservaciones(vPedido.taobservaciones.getText());
                pedido.setEstadoPedido((EstadoPedido) vPedido.cbEstadoPedido.getSelectedItem());
                pedido.setCliente(model.comprobarClienteCodigo(vPedido.lbcodigoCliente.getText()));

                model.guardardetallePedido(pedido,listarDestalles());

            }else{
                pedido.setCodigo(vPedido.tfCodigo.getText());
                pedido.setReferencia(vPedido.tfreferencia.getText());
                pedido.setNombre(vPedido.tfnombre.getText());
                pedido.setFecha_inicio(vPedido.Dateinicio.getDate());
                pedido.setFecha_fin(vPedido.Datefin.getDate());
                pedido.setImporte(Float.parseFloat(vPedido.tfimporte.getText()));
                pedido.setObservaciones(vPedido.taobservaciones.getText());
                pedido.setEstadoPedido((EstadoPedido) vPedido.cbEstadoPedido.getSelectedItem());
                pedido.setCliente(model.comprobarClienteCodigo(vPedido.lbcodigoCliente.getText()));

                model.guardardetallePedido(pedido,listarDestalles());
            }
        }else{
            if(model.getPedido(vPedido.tfCodigo.getText()) != null){
                JOptionPane.showMessageDialog(null,"El codigo ya existe , no habra cambios en este campo");
                String codigo = pedido.getCodigo();
                pedido.setCodigo(codigo);
                pedido.setReferencia(vPedido.tfreferencia.getText());
                pedido.setNombre(vPedido.tfnombre.getText());
                pedido.setFecha_inicio(vPedido.Dateinicio.getDate());
                pedido.setFecha_fin(vPedido.Datefin.getDate());
                pedido.setImporte(Float.parseFloat(vPedido.tfimporte.getText()));
                pedido.setObservaciones(vPedido.taobservaciones.getText());
                pedido.setEstadoPedido((EstadoPedido) vPedido.cbEstadoPedido.getSelectedItem());
                pedido.setCliente(model.comprobarClienteCodigo(vPedido.lbcodigoCliente.getText()));
                model.modificarHibernate(pedido);


            }else{
                pedido.setCodigo(vPedido.tfCodigo.getText());
                pedido.setReferencia(vPedido.tfreferencia.getText());
                pedido.setNombre(vPedido.tfnombre.getText());
                pedido.setFecha_inicio(vPedido.Dateinicio.getDate());
                pedido.setFecha_fin(vPedido.Datefin.getDate());
                pedido.setImporte(Float.parseFloat(vPedido.tfimporte.getText()));
                pedido.setObservaciones(vPedido.taobservaciones.getText());
                pedido.setEstadoPedido((EstadoPedido) vPedido.cbEstadoPedido.getSelectedItem());
                pedido.setCliente(model.comprobarClienteCodigo(vPedido.lbcodigoCliente.getText()));
                model.modificarHibernate(pedido);

            }
        }

    }


    public void buscarNombre(){

        vPedido.tfbuscarNombre.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vPedido.tfbuscarNombre.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vPedido.tfbuscarNombre.setText("Nombre...");
                listarTablaPedido();
            }
        });

        vPedido.tfbuscarNombre.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = vPedido.tfbuscarNombre.getText();
                List<Pedido> listaPedidos = model.buscarPedidoNombre(texto);
                dtmPedido.setRowCount(0);
                for(Pedido pedido : listaPedidos){
                    Object [] filaPedido = new Object[]{pedido.getCodigo(),pedido.getReferencia(),pedido.getNombre(),pedido.getFecha_inicio(),pedido.getFecha_fin(),
                            pedido.getImporte(),pedido.getEstadoPedido(),pedido.getObservaciones(),pedido.getCliente().getNombre()};
                    dtmPedido.addRow(filaPedido);
                }

            }
        });
    }

    public void buscarCodigo(){

        vPedido.tfbuscarCodigo.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vPedido.tfbuscarCodigo.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vPedido.tfbuscarCodigo.setText("Codigo...");
                listarTablaPedido();
            }
        });

        vPedido.tfbuscarCodigo.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = vPedido.tfbuscarCodigo.getText();
                List<Pedido> listaPedidos = model.buscarPedidoCodigo(texto);
                dtmPedido.setRowCount(0);
                for(Pedido pedido : listaPedidos){
                    Object [] filaPedido = new Object[]{pedido.getCodigo(),pedido.getReferencia(),pedido.getNombre(),pedido.getFecha_inicio(),pedido.getFecha_fin(),
                            pedido.getImporte(),pedido.getEstadoPedido(),pedido.getObservaciones(),pedido.getCliente().getNombre()};
                    dtmPedido.addRow(filaPedido);
                }

            }
        });
    }

    public void buscarReferencia(){

        vPedido.tfbuscarReferencia.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vPedido.tfbuscarReferencia.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vPedido.tfbuscarReferencia.setText("Referencia...");
                listarTablaPedido();
            }
        });

        vPedido.tfbuscarReferencia.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = vPedido.tfbuscarReferencia.getText();
                List<Pedido> listaPedidos = model.buscarPedidoReferencia(texto);
                dtmPedido.setRowCount(0);
                for(Pedido pedido : listaPedidos){
                    Object [] filaPedido = new Object[]{pedido.getCodigo(),pedido.getReferencia(),pedido.getNombre(),pedido.getFecha_inicio(),pedido.getFecha_fin(),
                            pedido.getImporte(),pedido.getEstadoPedido(),pedido.getObservaciones(),pedido.getCliente().getNombre()};
                    dtmPedido.addRow(filaPedido);
                }

            }
        });
    }



    public void buscarCliente(){

        vPedido.tfbuscarCliente.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vPedido.tfbuscarCliente.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vPedido.tfbuscarCliente.setText("Nombre Cliente...");
                listarTablaCliente();
            }
        });

        vPedido.tfbuscarCliente.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = vPedido.tfbuscarCliente.getText();
                List<Cliente> listaClientes = model.buscarPedidoCliente(texto);
                dtmCliente.setRowCount(0);
                for (Cliente cliente : listaClientes) {
                    Object[] filaCliente = new Object[]{cliente.getCodigo(), cliente.getNombre(), cliente.getDni(), cliente.getCif()};
                    dtmCliente.addRow(filaCliente);
                }
            }
        });
    }



    private void buscarDesdeFecha(){

        vPedido.datebuscarinicio.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                Date now = new Date();
                vPedido.datebuscarinicio.setDate(now);

            }

            @Override
            public void focusLost(FocusEvent e) {
                listarTablaPedido();
            }
        });

        Date inicio = vPedido.datebuscarinicio.getDate();
        try{
            java.sql.Date inicioSQL= Util.formateoDate(inicio);
            List<Pedido> listaPedidos = model.buscardesdeFecha(inicioSQL);
            dtmPedido.setRowCount(0);
            for(Pedido pedido : listaPedidos){
                Object [] filaPedido = new Object[]{pedido.getCodigo(),pedido.getReferencia(),pedido.getNombre(),pedido.getFecha_inicio(),pedido.getFecha_fin(),
                        pedido.getImporte(),pedido.getEstadoPedido(),pedido.getObservaciones(),pedido.getCliente().getNombre()};
                dtmPedido.addRow(filaPedido);
            }
        }catch(NullPointerException npe){
            vPedido.lbinfo.setText("No has seleccionado una fecha de busqueda");
        }

        }

    private void buscarEntreFechas(){

        vPedido.datebuscarinicio.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                Date now = new Date();
                vPedido.datebuscarinicio.setDate(now);

            }

            @Override
            public void focusLost(FocusEvent e) {
                listarTablaPedido();
            }
        });

        Date inicio = vPedido.datebuscarinicio.getDate();
        Date fin = vPedido.datebuscarFin.getDate();
        try{
            java.sql.Date inicioSQL= Util.formateoDate(inicio);
            java.sql.Date finSQL= Util.formateoDate(fin);

            List<Pedido> listaPedidos = model.buscarEntreFechas(inicioSQL,finSQL);
            dtmPedido.setRowCount(0);
            for(Pedido pedido : listaPedidos){
                Object [] filaPedido = new Object[]{pedido.getCodigo(),pedido.getReferencia(),pedido.getNombre(),pedido.getFecha_inicio(),pedido.getFecha_fin(),
                        pedido.getImporte(),pedido.getEstadoPedido(),pedido.getObservaciones(),pedido.getCliente().getNombre()};
                dtmPedido.addRow(filaPedido);
            }
        }catch(NullPointerException npe){
            vPedido.lbinfo.setText("No has seleccionado una fecha de busqueda");
        }

    }

    private void buscarEstadoPedido(){

        vPedido.cbbuscarEstado.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                vPedido.cbbuscarEstado = (JComboBox) e.getSource();

                Object estado = e.getItem();

                if(e.getStateChange() == ItemEvent.SELECTED){

                    if(vPedido.cbbuscarEstado.getSelectedIndex() <= 0){
                        listarTablaPedido();
                    }else{
                        vPedido.lbinfo.setText(estado.toString() + "Seleccionado");
                        String texto = estado.toString();
                        List<Pedido> listaPedidos = model.getListaTipoPedido(texto);
                        dtmPedido.setRowCount(0);
                        for(Pedido pedido : listaPedidos){
                            Object [] filaPedido = new Object[]{pedido.getCodigo(),pedido.getReferencia(),pedido.getNombre(),pedido.getFecha_inicio(),pedido.getFecha_fin(),
                                    pedido.getImporte(),pedido.getEstadoPedido(),pedido.getObservaciones(),pedido.getCliente().getNombre()};
                            dtmPedido.addRow(filaPedido);
                        }
                    }

                }
            }
        });
    }
}



