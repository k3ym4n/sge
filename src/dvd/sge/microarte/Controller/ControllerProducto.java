package dvd.sge.microarte.Controller;


import dvd.sge.microarte.base.DetalleProceso;
import dvd.sge.microarte.base.Proceso;
import dvd.sge.microarte.base.Producto;
import dvd.sge.microarte.face.Botonera;
import dvd.sge.microarte.face.VCliente;
import dvd.sge.microarte.face.vDetalleProceso;
import dvd.sge.microarte.face.vProducto;
import dvd.sge.microarte.util.Tintas;
import dvd.sge.microarte.util.TipoUsuario;
import dvd.sge.microarte.util.Util;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class ControllerProducto implements ActionListener {

    private vProducto vProducto;
    private Model model;
    private Botonera botonera;

    private DefaultTableModel dtmProducto,dtmDetalleProceso;
    private Producto producto;

    private vDetalleProceso vDetalleProceso;
    private ArrayList<DetalleProceso> detalleProcesos;

    private float tiempo;
    private Proceso proceso;
    private DetalleProceso detalleProceso;


    private String nickusuario = ControllerLogin.login.tfnick.getText();

    public ControllerProducto(vProducto vProducto, Model model, Botonera botonera) {
        this.vProducto = vProducto;
        this.model=model;
        this.botonera=botonera;

        detalleProcesos = new ArrayList<>();
        vProducto.lbnick.setText(nickusuario);
        NivelAcceso(model.comprobarUsuario(nickusuario).getTipoUsuario());

        listenerBotonera();
        modeloTablaProducto();
        modeloTablaDetalleProceso();
        listarComboBoxTintas();
        listarTablaProducto();
        listenerTablaProducto();
        buscarCodigo();
        buscarDenominacion();
        buscarReferencia();
        buscarImportedesde();
        listenerTablaProcesos();



        botonera.btmodificar.setEnabled(false);
        botonera.bteliminar.setEnabled(false);
    }

    private void NivelAcceso(TipoUsuario tipoUsuario) {

        switch (tipoUsuario) {
            case CONSULTA:
                vProducto.PanelBotonera.setVisible(false);
                vProducto.menuBar.setEnabled(false);
                vProducto.bteliminarproceso.setVisible(false);
                vProducto.btDetalleProceso.setVisible(false);


                break;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource()==botonera.btguardar){

            if(guardarCampoCodigo() == true  && camposObligatorios() == true){
                try{
                    guardarProducto();
                }catch(NumberFormatException nfe){
                    JOptionPane.showMessageDialog(null,"El formato del importe debe ser con punto Ej: 0.0");
                    vProducto.lbinfo.setText("El formato del importe debe ser con punto Ej: 0.0");
                }

                listarTablaProducto();
                listarComboBoxTintas();
                limpiarCampos();
            }


        }else if(e.getSource()==botonera.btmodificar){
            if(camposObligatorios() == true ){
            int filaProducto = vProducto.tablaProducto.getSelectedRow();
            String codigo = (String) vProducto.tablaProducto.getValueAt(filaProducto,0);
            Producto producto = model.getProducto(codigo);

                modificarProducto(producto);
                listarTablaProducto();
                listarComboBoxTintas();
                limpiarCampos();
                modeloTablaDetalleProceso();
                detalleProcesos.clear();
                botonera.btmodificar.setEnabled(false);
                botonera.bteliminar.setEnabled(false);
                botonera.btguardar.setEnabled(true);
                vProducto.btDetalleProceso.setEnabled(false);
                vProducto.bteliminarproceso.setEnabled(false);

            }

        }else if(e.getSource()==botonera.btcancelar){
            botonera.btguardar.setEnabled(true);
            botonera.btmodificar.setEnabled(false);
            botonera.bteliminar.setEnabled(false);
            limpiarCampos();
            modeloTablaDetalleProceso();
            detalleProcesos.clear();

        }else if(e.getSource()==botonera.bteliminar){

                int filaProducto = vProducto.tablaProducto.getSelectedRow();
                try{
                    String codigo = (String) vProducto.tablaProducto.getValueAt(filaProducto,0);
                    Producto producto = model.getProducto(codigo);

                    model.eliminarHibernate(producto);
                }catch(ArrayIndexOutOfBoundsException aiofbe){
                    vProducto.lbinfo.setText("Tienes que escoger un Producto para poder eliminarlo");
                }

                listarTablaProducto();
                listarComboBoxTintas();
                limpiarCampos();
                botonera.btguardar.setEnabled(true);
                botonera.btmodificar.setEnabled(false);
                botonera.bteliminar.setEnabled(false);

        }else if(e.getSource() == vProducto.btDetalleProceso){

            vDetalleProceso = new vDetalleProceso();
            vDetalleProceso.lbProducto.setText(vProducto.tfcodigo.getText());
            vDetalleProceso.visible();

            float tiempo = vDetalleProceso.getTiempo();
            Proceso proceso = vDetalleProceso.getProceso();
            Producto producto = vDetalleProceso.getProducto();

            DetalleProceso detalleProceso = new DetalleProceso();

            try{

                detalleProceso.setTiempo(tiempo);
                if(tiempo <= 1){
                    try{
                        detalleProceso.setImporte(tiempo * proceso.getPvp_minimo());
                        float precio = (tiempo * proceso.getPvp_minimo());
                        vProducto.tfimporte.setText(String.valueOf(Float.parseFloat(vProducto.tfimporte.getText()) + precio));
                    }catch(NullPointerException npe){
                        System.out.println("Has cerrado la ventana de procesos sin elegir proceso");
                    }

                }else if (tiempo > 1){
                    try{
                        detalleProceso.setImporte(tiempo * proceso.getPvp_hora());
                        float precio = (tiempo * proceso.getPvp_hora());
                        vProducto.tfimporte.setText(String.valueOf(Float.parseFloat(vProducto.tfimporte.getText()) + precio));
                    }catch(NumberFormatException nfe){
                       vProducto.lbinfo.setText("Has cerrado la ventana de procesos sin elegir proceso");
                    }catch (NullPointerException npe){
                        vProducto.lbinfo.setText("Has cerrado la ventana de procesos sin elegir proceso");
                    }

                }else if(tiempo == 0 ){
                    JOptionPane.showMessageDialog(null,"El campo tiempo esta vacio");
                    return;
                }


                detalleProceso.setProducto(producto);
                detalleProceso.setProceso(proceso);

                anadirDetalleProceso(detalleProceso);
                listarTablaAnadirDetalleProceso();

            }catch(NumberFormatException nfe){
                System.out.println("Has cerrado la ventana de procesos sin elegir proceso");
            }



        }else if(e.getSource() == vProducto.bteliminarproceso){
            int filaDetalleProceso = vProducto.tablaProcesos.getSelectedRow();
            int id = (int) vProducto.tablaProcesos.getValueAt(filaDetalleProceso,0);

            detalleProceso = model.getDetalleProcesoEliminar(id);

            float precio = detalleProceso.getImporte();
            detalleProceso.getProducto();
            detalleProceso.getProceso();

            vProducto.tfimporte.setText(String.valueOf(Float.parseFloat(vProducto.tfimporte.getText()) - precio));

            model.eliminarHibernate(detalleProceso);

            listarTablaDetalleProceso(detalleProceso.getProducto().getId());



        }

    }

    private void listenerBotonera() {
        botonera.btguardar.addActionListener(this);
        botonera.btmodificar.addActionListener(this);
        botonera.btcancelar.addActionListener(this);
        botonera.bteliminar.addActionListener(this);
        vProducto.btDetalleProceso.addActionListener(this);
        vProducto.bteliminarproceso.addActionListener(this);
        vProducto.btDetalleProceso.setEnabled(false);
        vProducto.bteliminarproceso.setEnabled(false);



    }

    private boolean camposObligatorios(){

        if( vProducto.tfreferencia.getText().isEmpty()){
            JOptionPane.showMessageDialog(null,"EL campo Referencias esta vacio ");
            return false;
        }else if(vProducto.tfdenominacion.getText().isEmpty()){
            JOptionPane.showMessageDialog(null,"EL campo denominacion esta vacio");
            return false;
        }else if(vProducto.cbtintas.getSelectedIndex() <= 0){
            JOptionPane.showMessageDialog(null,"EL campo Tintas esta vacio");
            return false;
        }else if(vProducto.tfimporte.getText().isEmpty() || Util.esNumero(vProducto.tfimporte.getText()) == false ){
            JOptionPane.showMessageDialog(null,"EL campo Importe esta vacio no es numerico");
            return false;
        }else if(vProducto.tpobservaciones.getText().isEmpty()){
            JOptionPane.showMessageDialog(null,"EL campo Observaciones esta vacio");
            vProducto.tpobservaciones.setText("Sin Observaciones");
            return true;
        }

        return true;
    }

    private void guardarProducto(){
        producto = new Producto();
        producto.setCodigo(vProducto.tfcodigo.getText());
        producto.setReferencia(vProducto.tfreferencia.getText());
        producto.setDenominacion(vProducto.tfdenominacion.getText());
        producto.setTintas((Tintas) vProducto.cbtintas.getSelectedItem());
        producto.setObservaciones(vProducto.tpobservaciones.getText());
        producto.setImporte(Float.parseFloat(vProducto.tfimporte.getText()));

        model.guardarHibernate(producto);

    }

    private boolean guardarCampoCodigo(){

        if(model.getProducto(vProducto.tfcodigo.getText()) != null || vProducto.tfcodigo.getText().isEmpty()){
            JOptionPane.showMessageDialog(null , "El codigo ya exite o esta vacio");
            return  false;
        }

        return true;

    }

    private void modificarProducto(Producto producto){

        if(vProducto.tablaProcesos.getRowCount() != 0){
            if(model.getProducto(vProducto.tfcodigo.getText()) != null){
                JOptionPane.showMessageDialog(null,"El codigo ya existe , no habra cambios en este campo");
                String codigo = producto.getCodigo();
                producto.setCodigo(codigo);
                producto.setReferencia(vProducto.tfreferencia.getText());
                producto.setDenominacion(vProducto.tfdenominacion.getText());
                producto.setTintas((Tintas) vProducto.cbtintas.getSelectedItem());
                producto.setImporte(Float.parseFloat(vProducto.tfimporte.getText()));
                producto.setObservaciones(vProducto.tpobservaciones.getText());
                model.guardarDetalleProceso(producto,listarDetalleProceso());

            }else{
                producto.setCodigo(vProducto.tfcodigo.getText());
                producto.setReferencia(vProducto.tfreferencia.getText());
                producto.setDenominacion(vProducto.tfdenominacion.getText());
                producto.setTintas((Tintas) vProducto.cbtintas.getSelectedItem());
                producto.setImporte(Float.parseFloat(vProducto.tfimporte.getText()));
                producto.setObservaciones(vProducto.tpobservaciones.getText());
                model.guardarDetalleProceso(producto,listarDetalleProceso());
            }
        }else{
            if(model.getProducto(vProducto.tfcodigo.getText()) != null){
                JOptionPane.showMessageDialog(null,"El codigo ya existe , no habra cambios en este campo");
                String codigo = producto.getCodigo();
                producto.setCodigo(codigo);
                producto.setReferencia(vProducto.tfreferencia.getText());
                producto.setDenominacion(vProducto.tfdenominacion.getText());
                producto.setTintas((Tintas) vProducto.cbtintas.getSelectedItem());
                producto.setImporte(Float.parseFloat(vProducto.tfimporte.getText()));
                producto.setObservaciones(vProducto.tpobservaciones.getText());
                model.modificarHibernate(producto);
            }else{
                producto.setCodigo(vProducto.tfcodigo.getText());
                producto.setReferencia(vProducto.tfreferencia.getText());
                producto.setDenominacion(vProducto.tfdenominacion.getText());
                producto.setTintas((Tintas) vProducto.cbtintas.getSelectedItem());
                producto.setImporte(Float.parseFloat(vProducto.tfimporte.getText()));
                producto.setObservaciones(vProducto.tpobservaciones.getText());
                model.modificarHibernate(producto);
            }
        }

    }

    private ArrayList<DetalleProceso> listarDetalleProceso(){
        return  detalleProcesos;
    }

    public void anadirDetalleProceso(DetalleProceso detalleProceso){
        detalleProcesos.add(detalleProceso);

    }

    private void listarTablaAnadirDetalleProceso(){
        List<DetalleProceso> listaProcesos = listarDetalleProceso();
        dtmDetalleProceso.setRowCount(0);
        try{
            for(DetalleProceso detalleProceso : listaProcesos){
                Object [] filaDetalle = new Object[]{ detalleProceso.getId(),detalleProceso.getProceso().getNombre(),detalleProceso.getTiempo(),detalleProceso.getProceso().getPvp_minimo(),
                        detalleProceso.getProceso().getPvp_hora(),detalleProceso.getImporte()};
                dtmDetalleProceso.addRow(filaDetalle);
            }
        }catch(NullPointerException npe){
            vProducto.lbinfo.setText("Has cerrado la ventana sin escoger producto");
            limpiarCampos();
        }



    }


    private void listarComboBoxTintas(){
        vProducto.cbtintas.removeAllItems();
        vProducto.cbtintas.addItem("Selecciona las Tintas");
        Tintas [] listatintas = Tintas.values();
        for(Tintas tintas : listatintas){
            vProducto.cbtintas.addItem(tintas);
        }
    }

    private void listarTablaProducto(){
        List<Producto> listaProductos = model.getListaProductos();
        dtmProducto.setRowCount(0);
        for(Producto producto : listaProductos){
            Object [] filaProducto = new Object[]{producto.getCodigo(),producto.getReferencia(),producto.getDenominacion(),producto.getTintas(),
            producto.getObservaciones(),producto.getImporte()};
            dtmProducto.addRow(filaProducto);
        }

    }

    private void listenerTablaProducto(){
        vProducto.tablaProducto.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try{
                    Producto producto = model.getProducto((String) vProducto.tablaProducto.getValueAt(vProducto.tablaProducto.getSelectedRow(),0));
                    vProducto.tfcodigo.setText(producto.getCodigo());
                    vProducto.tfreferencia.setText(String.valueOf(producto.getReferencia()));
                    vProducto.tfdenominacion.setText(producto.getDenominacion());
                    vProducto.cbtintas.setSelectedItem(producto.getTintas());
                    vProducto.tfimporte.setText(String.valueOf(producto.getImporte()));
                    vProducto.tpobservaciones.setText(producto.getObservaciones());

                    listarTablaDetalleProceso(producto.getId());
                    botonera.btguardar.setEnabled(false);
                    botonera.btmodificar.setEnabled(true);
                    botonera.bteliminar.setEnabled(true);

                    vProducto.btDetalleProceso.setEnabled(true);
                }catch (ArrayIndexOutOfBoundsException aiofb){
                    vProducto.lbinfo.setText("Error vuelve ha escoger un producto");
                }


            }
        });

    }

    private void listenerTablaProcesos(){
        vProducto.tablaProcesos.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vProducto.bteliminarproceso.setEnabled(true);
            }
        });
    }

    private void listarTablaDetalleProceso(int id){
        List<DetalleProceso> listaProcesos = model.getDetalleProcesoProducto(id);
        dtmDetalleProceso.setRowCount(0);
        for(DetalleProceso detalleProceso : listaProcesos){
            Object [] filalprocesos = new Object[]{
                    detalleProceso.getId(),
                    detalleProceso.getProceso().getNombre(),
                    detalleProceso.getTiempo(),
                    detalleProceso.getProceso().getPvp_minimo(),
                    detalleProceso.getProceso().getPvp_hora(),
                    detalleProceso.getImporte()};
            dtmDetalleProceso.addRow(filalprocesos);
        }

    }
    private void modeloTablaDetalleProceso(){
        dtmDetalleProceso = new DefaultTableModel();
        dtmDetalleProceso.addColumn("ID");
        dtmDetalleProceso.addColumn("Nombre");
        dtmDetalleProceso.addColumn("Tiempo");
        dtmDetalleProceso.addColumn("pvp/Minimo");
        dtmDetalleProceso.addColumn("pvp/Hora");
        dtmDetalleProceso.addColumn("Subtotal");
        vProducto.tablaProcesos.setModel(dtmDetalleProceso);
    }

    private void limpiarCampos(){
        vProducto.tfcodigo.setText("");
        vProducto.tfreferencia.setText("");
        vProducto.tfdenominacion.setText("");
        vProducto.cbtintas.setSelectedItem("");
        vProducto.tfimporte.setText("");
        vProducto.tpobservaciones.setText("");
        vProducto.cbtintas.setSelectedIndex(0);
        vProducto.lbinfo.setText("");

        vProducto.botonera.btmodificar.setEnabled(false);
        vProducto.botonera.btguardar.setEnabled(true);
        vProducto.btDetalleProceso.setEnabled(false);
        vProducto.bteliminarproceso.setEnabled(false);

    }

    private void modeloTablaProducto(){
        dtmProducto = new DefaultTableModel();
        dtmProducto.addColumn("Codigo");
        dtmProducto.addColumn("Referencia");
        dtmProducto.addColumn("Denominacion");
        dtmProducto.addColumn("Tintas");
        dtmProducto.addColumn("Observaciones");
        dtmProducto.addColumn("Importe");
        vProducto.tablaProducto.setModel(dtmProducto);
    }


    private void buscarCodigo(){

        vProducto.tfbuscarcodigo.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vProducto.tfbuscarcodigo.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vProducto.tfbuscarcodigo.setText("Codigo...");
                listarTablaProducto();
            }
        });

        vProducto.tfbuscarcodigo.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
               String texto = vProducto.tfbuscarcodigo.getText();
                List<Producto> listaProductos = model.buscarProductoCodigo(texto);
                dtmProducto.setRowCount(0);
                for(Producto producto : listaProductos){
                    Object [] filaProducto = new Object[]{producto.getCodigo(),producto.getReferencia(),producto.getDenominacion(),producto.getTintas(),
                            producto.getObservaciones(),producto.getImporte()};
                    dtmProducto.addRow(filaProducto);
                }
            }
        });

    }

    private void buscarReferencia(){

        vProducto.tfbuscarreferencia.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vProducto.tfbuscarreferencia.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vProducto.tfbuscarreferencia.setText("Referencia...");
                listarTablaProducto();
            }
        });

        vProducto.tfbuscarreferencia.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = vProducto.tfbuscarreferencia.getText();
                List<Producto> listaProductos = model.buscarProductoReferencia(texto);
                dtmProducto.setRowCount(0);
                for(Producto producto : listaProductos){
                    Object [] filaProducto = new Object[]{producto.getCodigo(),producto.getReferencia(),producto.getDenominacion(),producto.getTintas(),
                            producto.getObservaciones(),producto.getImporte()};
                    dtmProducto.addRow(filaProducto);
                }
            }
        });

    }

    private void buscarDenominacion(){

        vProducto.tfbuscardenominacion.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vProducto.tfbuscardenominacion.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vProducto.tfbuscardenominacion.setText("Denominacion...");
                listarTablaProducto();
            }
        });

        vProducto.tfbuscardenominacion.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = vProducto.tfbuscardenominacion.getText();
                List<Producto> listaProductos = model.buscarProductoDenominacion(texto);
                dtmProducto.setRowCount(0);
                for(Producto producto : listaProductos){
                    Object [] filaProducto = new Object[]{producto.getCodigo(),producto.getReferencia(),producto.getDenominacion(),producto.getTintas(),
                            producto.getObservaciones(),producto.getImporte()};
                    dtmProducto.addRow(filaProducto);
                }
            }
        });

    }



    private void buscarImportedesde(){

        vProducto.tfimportemin.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vProducto.tfimportemin.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {

                    vProducto.tfimportemin.setText("Importe min...");
                listarTablaProducto();


            }
        });

        vProducto.tfimportemin.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                try{
                    float minimo = Float.parseFloat(vProducto.tfimportemin.getText());
                    List<Producto> listaProductos = model.buscarProductoporPrecio(minimo);
                    dtmProducto.setRowCount(0);
                    for(Producto producto : listaProductos){
                        Object [] filaProducto = new Object[]{producto.getCodigo(),producto.getReferencia(),producto.getDenominacion(),producto.getTintas(),
                                producto.getObservaciones(),producto.getImporte()};
                        dtmProducto.addRow(filaProducto);
                    }
                }catch(NumberFormatException nfe){
                    vProducto.lbinfo.setText("El campo PVP Minimo... esta vacio");
                }

            }
        });



    }






}
