package dvd.sge.microarte.Controller;

import dvd.sge.microarte.base.Albaran;
import dvd.sge.microarte.base.Pedido;
import dvd.sge.microarte.face.Botonera;
import dvd.sge.microarte.face.VCliente;
import dvd.sge.microarte.face.vAlbaran;
import dvd.sge.microarte.util.TipoUsuario;
import dvd.sge.microarte.util.Util;


import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.util.Date;
import java.util.List;

public class ControllerAlbaran implements ActionListener {

    private vAlbaran vAlbaran;
    private Model model;
    private Botonera botonera;
    private DefaultTableModel dtmAlbaran, dtmPedido;
    private Albaran albaran;

    private String nickusuario = ControllerLogin.login.tfnick.getText();

    public ControllerAlbaran(vAlbaran vAlbaran, Model model, Botonera botonera) {
        this.vAlbaran = vAlbaran;
        this.model = model;
        this.botonera = botonera;

        listenerbotones();
        modeloTablas();
        listarTablaPedido();
        listenerTablaPedido();
        listarTablapresupuestos();
        listenerTablaPresupuesto();
        buscarPedido();
        buscarPresupuesto();
        buscarDesdeFecha();
        buscarEntreFechas();

        vAlbaran.lbnick.setText(nickusuario);
        nivelAccesoUsuario(model.comprobarUsuario(nickusuario).getTipoUsuario());

        botonera.btmodificar.setEnabled(false);
        botonera.bteliminar.setEnabled(false);

    }

    private void nivelAccesoUsuario(TipoUsuario tipoUsuario) {

        switch (tipoUsuario) {
            case CONSULTA:
                vAlbaran.panelBotonera.setVisible(false);
                vAlbaran.menuBar.setEnabled(false);

                break;
        }
    }

    private void listenerbotones() {
        botonera.btguardar.addActionListener(this);
        botonera.btmodificar.addActionListener(this);
        botonera.btcancelar.addActionListener(this);
        botonera.bteliminar.addActionListener(this);
        vAlbaran.btbuscarFechas.addActionListener(this);
        vAlbaran.btrefrescar.addActionListener(this);
    }

    private void limpiarCampos(){
        vAlbaran.tfimporte.setText("");
        vAlbaran.tpobservaciones.setText("");
        vAlbaran.lbnumeroPresupuesto.setText("");
        vAlbaran.lbcodigoPedido.setText("");
        vAlbaran.lbimportePedido.setText("");
        vAlbaran.lbNombreCliente.setText("");
        vAlbaran.lbNombrePedido.setText("");
        vAlbaran.lbinfo.setText("");


    }

    private void listenerTablaPedido(){
        vAlbaran.tablaPedidos.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Pedido pedido = model.getPedido((String) vAlbaran.tablaPedidos.getValueAt(vAlbaran.tablaPedidos.getSelectedRow(),0));
                vAlbaran.lbNombreCliente.setText(pedido.getCliente().getNombre());
                vAlbaran.lbnumeroPresupuesto.setText(String.valueOf(pedido.getId()));
                vAlbaran.lbcodigoPedido.setText(pedido.getCodigo());
                vAlbaran.lbNombrePedido.setText(pedido.getNombre());
                vAlbaran.lbimportePedido.setText(String.valueOf(pedido.getImporte()));
                vAlbaran.tfimporte.setText(String.valueOf(pedido.getImporte()));
            }
        });
    }

    private void listenerTablaPresupuesto(){
        vAlbaran.tablaPresupuestos.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try{
                    Albaran albaran = model.getPresupuesto((Integer) vAlbaran.tablaPresupuestos.getValueAt(vAlbaran.tablaPresupuestos.getSelectedRow(),0));
                    vAlbaran.tpobservaciones.setText(albaran.getObservaciones());
                    vAlbaran.tfimporte.setText(String.valueOf(albaran.getImporte()));

                    vAlbaran.lbNombreCliente.setText(albaran.getPedido().getCliente().getNombre());
                    vAlbaran.lbnumeroPresupuesto.setText(String.valueOf(albaran.getPedido().getId()));
                    vAlbaran.lbcodigoPedido.setText(albaran.getPedido().getCodigo());
                    vAlbaran.lbNombrePedido.setText(albaran.getPedido().getNombre());
                    vAlbaran.lbimportePedido.setText(String.valueOf(albaran.getPedido().getImporte()));

                    botonera.btguardar.setEnabled(false);
                    botonera.btmodificar.setEnabled(true);
                    botonera.bteliminar.setEnabled(true);
                }catch(ArrayIndexOutOfBoundsException aibex){
                    vAlbaran.lbinfo.setText("Tienes que escoger un Presupuesto");
                }


            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource() == botonera.btguardar){
            if(camposObligatorios() == true){
                try{
                    guardarAlbaran();
                }catch(NumberFormatException nfe){
                    vAlbaran.lbinfo.setText("El formato del numero debe ser 0.0 , recuerde el punto");

                }

                listarTablapresupuestos();
                limpiarCampos();
            }


        }else if(e.getSource() == botonera.btmodificar){
            try{
                if(camposObligatorios() == true){
                    int filaPresupuesto = vAlbaran.tablaPresupuestos.getSelectedRow();
                    int numPedido = (int) vAlbaran.tablaPresupuestos.getValueAt(filaPresupuesto,0);
                    Albaran presupuesto = model.getPresupuesto(numPedido);
                    modificarAlbaran(presupuesto);
                    limpiarCampos();

                    botonera.btmodificar.setEnabled(false);
                    botonera.bteliminar.setEnabled(false);
                    botonera.btguardar.setEnabled(true);
                }
            }catch (ArrayIndexOutOfBoundsException aiobe){
                vAlbaran.lbinfo.setText("Debes escoger Presupuesto de la tabla Usuarios");
            }

            listarTablapresupuestos();

        }else if(e.getSource() == botonera.bteliminar){

            try{

                    int filaPresupuesto = vAlbaran.tablaPresupuestos.getSelectedRow();
                    int numPedido = (int) vAlbaran.tablaPresupuestos.getValueAt(filaPresupuesto,0);
                    Albaran presupuesto = model.getPresupuesto(numPedido);
                     model.eliminarHibernate(presupuesto);
                    limpiarCampos();
                    listarTablapresupuestos();

                    botonera.btmodificar.setEnabled(false);
                    botonera.bteliminar.setEnabled(false);

            }catch (ArrayIndexOutOfBoundsException aiobe){
                vAlbaran.lbinfo.setText("Debes escoger Presupuesto de la tabla Usuarios");
            }



        }else if(e.getSource() == botonera.btcancelar){
            limpiarCampos();
            listarTablapresupuestos();
            listarTablaPedido();
            botonera.btguardar.setEnabled(true);
            botonera.btmodificar.setEnabled(false);
            botonera.bteliminar.setEnabled(false);

        }else if(e.getSource() == vAlbaran.btbuscarFechas){

            if(vAlbaran.fechaInicio.getDate() != null && vAlbaran.fechaFin.getDate() != null){
                buscarEntreFechas();
                vAlbaran.fechaInicio.setDate(null);
                vAlbaran.fechaFin.setDate(null);


            }else if(vAlbaran.fechaInicio.getDate() != null && vAlbaran.fechaFin.getDate() == null){
                buscarDesdeFecha();
                vAlbaran.fechaInicio.setDate(null);

            }else if(vAlbaran.fechaFin.getDate() != null && vAlbaran.fechaInicio.getDate() == null){
                vAlbaran.lbinfo.setText("No se puede realizar una consulta sin una fecha inicial de busqueda");
                vAlbaran.fechaFin.setDate(null);
            }else{
                vAlbaran.lbinfo.setText("Nesesitas al menos un campo fecha");
            }

        }else if(e.getSource() == vAlbaran.btrefrescar){
            listarTablaPedido();
        }

    }

    private boolean camposObligatorios(){

        if(vAlbaran.lbcodigoPedido.getText().isEmpty()){
            JOptionPane.showMessageDialog(null , "Debes de escoger un Pedido");
            return false;
        }else if(model.getPresupuesto(Integer.parseInt(vAlbaran.lbnumeroPresupuesto.getText())) != null){
            JOptionPane.showMessageDialog(null,"Este pedido ya tiene un presupuesto");
            return false;
        }else if(vAlbaran.tpobservaciones.getText().isEmpty()){
            vAlbaran.tpobservaciones.setText("Sin Observaciones");

            return true;
        }

        return true;
    }


    private void guardarAlbaran(){
        albaran = new Albaran();

        Date date = new Date();
        albaran.setFecha(date);
        albaran.setImporte(Float.parseFloat(vAlbaran.tfimporte.getText()));
        albaran.setNumpresupuesto(Integer.parseInt(vAlbaran.lbnumeroPresupuesto.getText()));
        albaran.setObservaciones(vAlbaran.tpobservaciones.getText());
        albaran.setPedido(model.getPedido(vAlbaran.lbcodigoPedido.getText()));

        model.guardarHibernate(albaran);


    }

    private void modificarAlbaran(Albaran albaran){


        if(model.getPresupuesto(Integer.parseInt(vAlbaran.lbnumeroPresupuesto.getText())) != null){
           JOptionPane.showMessageDialog(null,"Este pedido ya tiene un presupuesto");
           return;
        }else {
            Date fecha = albaran.getFecha();
            albaran.setFecha(fecha);
            albaran.setImporte(Float.parseFloat(vAlbaran.tfimporte.getText()));
            albaran.setNumpresupuesto(Integer.parseInt(vAlbaran.lbnumeroPresupuesto.getText()));
            albaran.setObservaciones(vAlbaran.tpobservaciones.getText());
            albaran.setPedido(model.getPedido(vAlbaran.lbcodigoPedido.getText()));
            model.modificarHibernate(albaran);
        }


    }


    private void listarTablaPedido(){
        List<Pedido> listaPedido = model.getListaPedidos();
        dtmPedido.setRowCount(0);
        for(Pedido pedido : listaPedido){
            Object [] filaPedido = new Object[]{pedido.getCodigo(),pedido.getNombre(),pedido.getImporte(),pedido.getCliente().getNombre()};
            dtmPedido.addRow(filaPedido);

        }
    }

    private void listarTablapresupuestos(){
        List<Albaran> listaAlbaran = model.getPresupuesto();
        dtmAlbaran.setRowCount(0);
        for(Albaran albaran : listaAlbaran){
            Object [] filaAlbaran = new Object[]{albaran.getNumpresupuesto(),albaran.getFecha(),albaran.getImporte(),
            albaran.getObservaciones(),albaran.getPedido().getCliente().getNombre()};
            dtmAlbaran.addRow(filaAlbaran);
        }
    }



    private void modeloTablas(){
        dtmAlbaran = new DefaultTableModel();
        dtmAlbaran.addColumn("Numero");
        dtmAlbaran.addColumn("Fecha Emision");
        dtmAlbaran.addColumn("Importe");
        dtmAlbaran.addColumn("Observaciones");
        dtmAlbaran.addColumn("Cliente");
        vAlbaran.tablaPresupuestos.setModel(dtmAlbaran);

        dtmPedido = new DefaultTableModel();
        dtmPedido.addColumn("Codigo");
        dtmPedido.addColumn("Nombre");
        dtmPedido.addColumn("Imorte");
        dtmPedido.addColumn("Cliente");
        vAlbaran.tablaPedidos.setModel(dtmPedido);

    }


    public void buscarPresupuesto(){

        vAlbaran.tfbuscarPresupuesto.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vAlbaran.tfbuscarPresupuesto.setText("");
                vAlbaran.lbinfo.setText("Introduce el nombre completo del Cliente");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vAlbaran.tfbuscarPresupuesto.setText("Presupuesto...");
                listarTablapresupuestos();
            }
        });


        vAlbaran.tfbuscarPresupuesto.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = vAlbaran.tfbuscarPresupuesto.getText();
                System.out.println(texto);
                List<Albaran> listaAlbaran = model.buscarPresupuesto(texto);
                dtmAlbaran.setRowCount(0);
                for(Albaran albaran : listaAlbaran){
                    Object [] filaAlbaran = new Object[]{albaran.getNumpresupuesto(),albaran.getFecha(),albaran.getImporte(),
                            albaran.getObservaciones(),albaran.getPedido().getCliente().getNombre()};
                    dtmAlbaran.addRow(filaAlbaran);
                    System.out.println(filaAlbaran);
                }

            }
        });

    }

    public void buscarPedido(){

        vAlbaran.tfbuscarPedido.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vAlbaran.tfbuscarPedido.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vAlbaran.tfbuscarPedido.setText("Pedido...");
                listarTablapresupuestos();
            }
        });


        vAlbaran.tfbuscarPedido.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = vAlbaran.tfbuscarPedido.getText();
                List<Pedido> listaPedido = model.buscarPedidoCodigo(texto);
                dtmPedido.setRowCount(0);
                for(Pedido pedido : listaPedido){
                    Object [] filaPedido = new Object[]{pedido.getCodigo(),pedido.getNombre(),pedido.getImporte(),pedido.getCliente().getNombre()};
                    dtmPedido.addRow(filaPedido);

                }

            }
        });

    }



    private void buscarDesdeFecha(){

        vAlbaran.fechaInicio.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                Date now = new Date();
                vAlbaran.fechaInicio.setDate(now);

            }

            @Override
            public void focusLost(FocusEvent e) {
                listarTablaPedido();
            }
        });

        Date inicio = vAlbaran.fechaInicio.getDate();
        try{
            java.sql.Date inicioSQL= Util.formateoDate(inicio);
            List<Albaran> listaAlbaran = model.buscardesdeFechaAlbaran(inicioSQL);
            dtmAlbaran.setRowCount(0);
            for(Albaran albaran : listaAlbaran){
                Object [] filaAlbaran = new Object[]{albaran.getNumpresupuesto(),albaran.getFecha(),albaran.getImporte(),
                        albaran.getObservaciones(),albaran.getPedido().getCliente().getNombre()};
                dtmAlbaran.addRow(filaAlbaran);
            }


        }catch(NullPointerException npe){
            vAlbaran.lbinfo.setText("No has seleccionado una fecha de busqueda");
        }

    }

    private void buscarEntreFechas(){

        vAlbaran.fechaFin.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                Date now = new Date();
                vAlbaran.fechaFin.setDate(now);

            }

            @Override
            public void focusLost(FocusEvent e) {
                listarTablaPedido();
            }
        });

        Date inicio = vAlbaran.fechaInicio.getDate();
        Date fin = vAlbaran.fechaFin.getDate();
        try{
            java.sql.Date inicioSQL= Util.formateoDate(inicio);
            java.sql.Date finSQL= Util.formateoDate(fin);

            List<Albaran> listaAlbaran = model.buscarEntreFechasAlbaran(inicioSQL,finSQL);
            dtmAlbaran.setRowCount(0);
            for(Albaran albaran : listaAlbaran){
                Object [] filaAlbaran = new Object[]{albaran.getNumpresupuesto(),albaran.getFecha(),albaran.getImporte(),
                        albaran.getObservaciones(),albaran.getPedido().getCliente().getNombre()};
                dtmAlbaran.addRow(filaAlbaran);
            }



        }catch(NullPointerException npe){
            vAlbaran.lbinfo.setText("No has seleccionado una fecha de busqueda");
        }

    }

}
