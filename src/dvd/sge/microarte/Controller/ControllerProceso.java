package dvd.sge.microarte.Controller;

import dvd.sge.microarte.base.Proceso;
import dvd.sge.microarte.face.Botonera;
import dvd.sge.microarte.face.vProceso;
import dvd.sge.microarte.util.TipoUsuario;
import dvd.sge.microarte.util.Util;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.util.List;

public class ControllerProceso implements ActionListener {

    private vProceso vProceso;
    private Model model;
    private Botonera botonera;

    private DefaultTableModel dtmProceso;
    private Proceso proceso;

    private String nickusuario = ControllerLogin.login.tfnick.getText();


    public ControllerProceso(vProceso vProceso, Model model, Botonera botonera) {
        this.vProceso = vProceso;
        this.model=model;
        this.botonera=botonera;

        listenerBotonera();
        modeloTablaProceso();
        listarTablaProceso();
        listenerTablaProceso();

        buscarCodigo();
        buscarNombre();
        buscarPVPhora();
        buscarPVPminimo();
        vProceso.lbnick.setText(nickusuario);
        NivelAcceso(model.comprobarUsuario(nickusuario).getTipoUsuario());
        botonera.btmodificar.setEnabled(false);
        botonera.bteliminar.setEnabled(false);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource()==botonera.btguardar){

            if(guardarCampoCodigo() == true && comprobarCamposObligatorios() == true){
                try{
                    guardarProceso();
                }catch (NumberFormatException nfe){
                    vProceso.lbinfo.setText("El formato de pvp/minimo o pvp/hora no es correcto. Ej: 0.0");
                }

                limpiarCampos();
                listarTablaProceso();

            }

        }else if(e.getSource()==botonera.btmodificar){
            if(modificarCodigo() == true && comprobarCamposObligatorios() == true){
            int FilaProceso = vProceso.tablaProceso.getSelectedRow();
            String codigo = (String) vProceso.tablaProceso.getValueAt(FilaProceso,0);
            Proceso proceso = model.getProceso(codigo);

                modificarProceso(proceso);
                limpiarCampos();
                listarTablaProceso();
                botonera.btguardar.setEnabled(true);
                botonera.btmodificar.setEnabled(false);
                botonera.bteliminar.setEnabled(false);
            }



        }else if(e.getSource()==botonera.btcancelar){
            limpiarCampos();
            botonera.btguardar.setEnabled(true);
            botonera.btmodificar.setEnabled(false);
            botonera.bteliminar.setEnabled(false);

        }else if(e.getSource()==botonera.bteliminar){

                int FilaProceso = vProceso.tablaProceso.getSelectedRow();
                String codigo = (String) vProceso.tablaProceso.getValueAt(FilaProceso, 0);
                Proceso proceso = model.getProceso(codigo);
                model.eliminarHibernate(proceso);
                listarTablaProceso();
                botonera.btguardar.setEnabled(true);
                botonera.bteliminar.setEnabled(false);
                botonera.btmodificar.setEnabled(false);


        }

    }

    private void listenerBotonera(){
        botonera.btguardar.addActionListener(this);
        botonera.btmodificar.addActionListener(this);
        botonera.btcancelar.addActionListener(this);
        botonera.bteliminar.addActionListener(this);
    }

    private void modeloTablaProceso(){
        dtmProceso = new DefaultTableModel();
        dtmProceso.addColumn("Codigo");
        dtmProceso.addColumn("Nombre");
        dtmProceso.addColumn("PVP/Minimo");
        dtmProceso.addColumn("PVP/Hora");
        dtmProceso.addColumn("Observaciones");
        vProceso.tablaProceso.setModel(dtmProceso);

    }

    private void listenerTablaProceso(){
        vProceso.tablaProceso.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Proceso proceso = model.getProceso((String) vProceso.tablaProceso.getValueAt(vProceso.tablaProceso.getSelectedRow(),0));
                vProceso.tfnombre.setText(proceso.getNombre());
                vProceso.tfpvpminimo.setText(String.valueOf(proceso.getPvp_minimo()));
                vProceso.tfpvphora.setText(String.valueOf(proceso.getPvp_hora()));

                vProceso.tpObservaciones.setText(proceso.getObservaciones());

                botonera.btguardar.setEnabled(false);
                botonera.btmodificar.setEnabled(true);
                botonera.bteliminar.setEnabled(true);

            }
        });
    }

    private void guardarProceso(){
        proceso = new Proceso();

        proceso.setCodigo(vProceso.tfcodigo.getText());
        proceso.setNombre(vProceso.tfnombre.getText());
        proceso.setPvp_minimo(Float.parseFloat(vProceso.tfpvpminimo.getText()));
        proceso.setPvp_hora(Float.parseFloat(vProceso.tfpvphora.getText()));
        proceso.setObservaciones(vProceso.tpObservaciones.getText());

        model.guardarHibernate(proceso);

    }

    private void modificarProceso(Proceso proceso){

        if(vProceso.tfcodigo.getText().isEmpty()){
            String codigo = proceso.getCodigo();
            proceso.setCodigo(codigo);
            model.modificarHibernate(proceso);

        }else{
            proceso.setCodigo(vProceso.tfcodigo.getText());
            model.modificarHibernate(proceso);
        }

        proceso.setNombre(vProceso.tfnombre.getText());
        proceso.setPvp_minimo(Float.parseFloat(vProceso.tfpvpminimo.getText()));
        proceso.setPvp_hora(Float.parseFloat(vProceso.tfpvphora.getText()));
        proceso.setObservaciones(vProceso.tpObservaciones.getText());

        model.modificarHibernate(proceso);

    }


    private boolean comprobarCamposObligatorios(){

        if(vProceso.tfnombre.getText().isEmpty()){
            JOptionPane.showMessageDialog(null,"El campo Nombre esta vacio ");
            return false;
        }else if(vProceso.tfpvpminimo.getText().isEmpty() || Util.esNumero(vProceso.tfpvpminimo.getText()) == false){
            JOptionPane.showMessageDialog(null,"El campo PVP/Minimo esta vacio ");
            return false;
        }else if(vProceso.tfpvphora.getText().isEmpty() || Util.esNumero(vProceso.tfpvphora.getText()) == false){
            JOptionPane.showMessageDialog(null,"El campo PVP/Hora esta vacio ");
            return false;
        }else if(vProceso.tpObservaciones.getText().isEmpty()){
            vProceso.tpObservaciones.setText("Sin Observaciones");
            return true;
        }

        return true;
    }

    private boolean guardarCampoCodigo(){

        if(vProceso.tfcodigo.getText().isEmpty() || model.getProceso(vProceso.tfcodigo.getText()) != null){
            JOptionPane.showMessageDialog(null,"El campo codigo esta vacio o ya existe");
            return false;

        }
        return true;
    }

    private boolean modificarCodigo(){
        if(model.getProceso(vProceso.tfcodigo.getText()) != null){
            JOptionPane.showMessageDialog(null,"El campo codigo ya existe");
            return false;
        }

        return true;
    }

    private void listarTablaProceso(){
        List<Proceso> listaDprocesos = model.getListaProcesos();
        dtmProceso.setRowCount(0);
        for(Proceso proceso : listaDprocesos){
            Object [] filaProceso = new Object[]{proceso.getCodigo(),proceso.getNombre(),proceso.getPvp_minimo(),proceso.getPvp_hora(),proceso.getObservaciones()};
            dtmProceso.addRow(filaProceso);
        }

    }

    private void limpiarCampos(){
        vProceso.tfcodigo.setText("");
        vProceso.tfnombre.setText("");
        vProceso.tfpvpminimo.setText("");
        vProceso.tfpvphora.setText("");
        vProceso.tpObservaciones.setText("");
        vProceso.lbinfo.setText("");


    }

    private void NivelAcceso(TipoUsuario tipoUsuario){

        switch (tipoUsuario) {
            case CONSULTA:
                vProceso.PanelBotonera.setVisible(false);
                vProceso.menuBar.setEnabled(false);

                break;
        }

    }


    private void buscarCodigo(){

        vProceso.tfbuscarcodigo.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vProceso.tfbuscarcodigo.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vProceso.tfbuscarcodigo.setText("Codigo...");
                listarTablaProceso();

            }
        });

        vProceso.tfbuscarcodigo.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = vProceso.tfbuscarcodigo.getText();
                List<Proceso> listaDprocesos = model.buscarProcesoCodigo(texto);
                dtmProceso.setRowCount(0);
                for(Proceso proceso : listaDprocesos){
                    Object [] filaProceso = new Object[]{proceso.getCodigo(),proceso.getNombre(),proceso.getPvp_minimo(),proceso.getPvp_hora(),proceso.getObservaciones()};
                    dtmProceso.addRow(filaProceso);
                }
            }
        });
    }


    private void buscarNombre(){

        vProceso.tfbuscarnombre.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vProceso.tfbuscarnombre.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vProceso.tfbuscarnombre.setText("Nombre...");
                listarTablaProceso();

            }
        });

        vProceso.tfbuscarnombre.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = vProceso.tfbuscarnombre.getText();
                List<Proceso> listaDprocesos = model.buscarProcesoNombre(texto);
                dtmProceso.setRowCount(0);
                for(Proceso proceso : listaDprocesos){
                    Object [] filaProceso = new Object[]{proceso.getCodigo(),proceso.getNombre(),proceso.getPvp_minimo(),proceso.getPvp_hora(),proceso.getObservaciones()};
                    dtmProceso.addRow(filaProceso);
                }
            }
        });
    }

    private void buscarPVPminimo(){

        vProceso.tfbuscarpvpminimo.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vProceso.tfbuscarpvpminimo.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vProceso.tfbuscarpvpminimo.setText("PVP/Minimo...");
                listarTablaProceso();

            }
        });

        vProceso.tfbuscarpvpminimo.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                try{
                    float texto = Float.parseFloat(vProceso.tfbuscarpvpminimo.getText());
                    List<Proceso> listaDprocesos = model.buscarProcesoPVPminimo(texto);
                    dtmProceso.setRowCount(0);
                    for(Proceso proceso : listaDprocesos){
                        Object [] filaProceso = new Object[]{proceso.getCodigo(),proceso.getNombre(),proceso.getPvp_minimo(),proceso.getPvp_hora(),proceso.getObservaciones()};
                        dtmProceso.addRow(filaProceso);
                    }
                }catch(NumberFormatException nfe){
                    vProceso.lbinfo.setText("El campo PVP/Minimo esta vacio");
                }

            }
        });
    }

    private void buscarPVPhora(){

        vProceso.tfbuscarpvphora.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vProceso.tfbuscarpvphora.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vProceso.tfbuscarpvphora.setText("PVP/Hora...");
                listarTablaProceso();

            }
        });

        vProceso.tfbuscarpvphora.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                try{
                    float texto = Float.parseFloat(vProceso.tfbuscarpvphora.getText());
                    List<Proceso> listaDprocesos = model.buscarProcesoPVPhora(texto);
                    dtmProceso.setRowCount(0);
                    for(Proceso proceso : listaDprocesos){
                        Object [] filaProceso = new Object[]{proceso.getCodigo(),proceso.getNombre(),proceso.getPvp_minimo(),proceso.getPvp_hora(),proceso.getObservaciones()};
                        dtmProceso.addRow(filaProceso);
                    }
                }catch(NumberFormatException nfe){
                    vProceso.lbinfo.setText("El campo PVP/Hora esta vacio");
                }
            }
        });
    }
}
