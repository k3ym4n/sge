package dvd.sge.microarte.Controller;

import dvd.sge.microarte.face.*;
import dvd.sge.microarte.util.Topaccion;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ControllerTopAccion {
    private Topaccion topaccion;
    private VLogin login;
    private Principal vprincipal;
    private Registro registro;
    private VCliente vCliente;
    private vProceso vProceso;
    private vProducto vProducto;
    private vPedido vPedido;
    private vAlbaran vAlbaran;

    private vDetallePedido detallePedido;
    private vDetalleProceso detalleProceso;





    public ControllerTopAccion(VLogin login, Topaccion topaccion){
        this.topaccion = topaccion;
        this.login = login;
        topaccion.lbminimizar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                login.frame.setExtendedState(Frame.ICONIFIED);

            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizarblack.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizar.png")));
            }
        });


        topaccion.lbexit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exitrojo.png")));
            }
            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exit.png")));
            }


        });
    }

    public ControllerTopAccion(Registro registro, Topaccion topaccion){
        this.topaccion = topaccion;
        this.registro = registro;
        topaccion.lbminimizar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                registro.frame.setExtendedState(Frame.ICONIFIED);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizarblack.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizar.png")));
            }


        });
        topaccion.lbexit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                registro.frame.setVisible(false);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exitrojo.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exit.png")));
            }
        });

    }

    public ControllerTopAccion(VCliente vCliente,Topaccion topaccion){
        this.vCliente= vCliente;
        this.topaccion=topaccion;

        topaccion.lbminimizar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vCliente.frame.setExtendedState(Frame.ICONIFIED);

            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizarblack.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizar.png")));
            }


        });

        topaccion.lbexit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vCliente.frame.setVisible(false);
            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exitrojo.png")));
            }
            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exit.png")));
            }
        });


    }



    public ControllerTopAccion(vProceso vProceso, Topaccion acciontop) {
        this.vProceso = vProceso;
        this.topaccion = acciontop;

        topaccion.lbminimizar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vProceso.frame.setExtendedState(Frame.ICONIFIED);

            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizarblack.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizar.png")));
            }
        });

        topaccion.lbexit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vProceso.frame.setVisible(false);
            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exitrojo.png")));
            }
            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exit.png")));
            }
        });
    }


    public ControllerTopAccion(vProducto vProducto, Topaccion accionTop) {
        this.vProducto = vProducto;
        this.topaccion = accionTop;

        topaccion.lbminimizar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vProducto.frame.setExtendedState(Frame.ICONIFIED);


            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizarblack.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizar.png")));
            }
        });

        topaccion.lbexit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vProducto.frame.setVisible(false);
            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exitrojo.png")));
            }
            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exit.png")));
            }
        });
    }


    public ControllerTopAccion(vPedido vPedido, Topaccion topAccion) {
        this.vPedido = vPedido;
        this.topaccion=topAccion;

        topaccion.lbminimizar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vPedido.frame.setExtendedState(Frame.ICONIFIED);


            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizarblack.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizar.png")));
            }
        });

        topaccion.lbexit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vPedido.frame.setVisible(false);
            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exitrojo.png")));
            }
            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exit.png")));
            }
        });
    }

    public ControllerTopAccion(vAlbaran vAlbaran, Topaccion topaccion) {
        this.vAlbaran = vAlbaran;
        this.topaccion = topaccion;


        topaccion.lbminimizar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vAlbaran.frame.setExtendedState(Frame.ICONIFIED);


            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizarblack.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizar.png")));
            }
        });

        topaccion.lbexit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vAlbaran.frame.setVisible(false);
            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exitrojo.png")));
            }
            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exit.png")));
            }
        });
    }




    public ControllerTopAccion(Principal principal, Topaccion accionTop, Registro registro) {
        this.vprincipal = principal;
        this.topaccion = accionTop;
        this.registro = registro;


        topaccion.lbminimizar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vprincipal.frame.setExtendedState(Frame.ICONIFIED);


            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizarblack.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizar.png")));
            }
        });


        topaccion.lbexit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                vprincipal.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                vprincipal.frame.setVisible(false);

                login = new VLogin();

            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exitrojo.png")));
            }
            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exit.png")));
            }
        });

    }

    public ControllerTopAccion(vDetallePedido vdetallePedido, Topaccion accionTop) {
        this.detallePedido = vdetallePedido;
        this.topaccion = accionTop;


        topaccion.lbminimizar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vdetallePedido.setVisible(false);


            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizarblack.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizar.png")));
            }
        });

        topaccion.lbexit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vdetallePedido.setVisible(false);
            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exitrojo.png")));
            }
            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exit.png")));
            }
        });
    }


    public ControllerTopAccion(vDetalleProceso vDetalleProceso, Topaccion accionTop) {
        this.detalleProceso = vDetalleProceso;
        this.topaccion = accionTop;


        topaccion.lbminimizar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vDetalleProceso.setVisible(false);


            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizarblack.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbminimizar.setIcon(new ImageIcon(getClass().getResource("/img/minimizar.png")));
            }
        });

        topaccion.lbexit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vDetalleProceso.setVisible(false);
            }
            @Override
            public void mouseEntered(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exitrojo.png")));
            }
            @Override
            public void mouseExited(MouseEvent e) {
                topaccion.lbexit.setIcon(new ImageIcon(getClass().getResource("/img/exit.png")));
            }
        });


    }
}









