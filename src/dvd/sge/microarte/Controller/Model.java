package dvd.sge.microarte.Controller;

import dvd.sge.microarte.base.*;
import dvd.sge.microarte.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Model {



    public Model() {



        conectar();
    }

    public void conectar() {

        HibernateUtil.buildSessionFactory();
        HibernateUtil.openSession();
    }

    public void desconectar() {
        HibernateUtil.closeSessionFactory();
    }

    /*****************************************Ventana Login****************************************/

    public Usuario logeandose(String nick,String pass){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuario WHERE nick=:nick and password=:pass");
        query.setParameter("nick",nick);
        query.setParameter("pass",pass);
        Usuario usuario = (Usuario) query.uniqueResult();
        return usuario;
    }

    public List<Usuario> getListaUsuarios() {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuario ");
        ArrayList<Usuario> usuarios = (ArrayList<Usuario>) query.list();
        return usuarios;
    }

    public List<Cliente> getListaClientes(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Cliente ");
        ArrayList<Cliente> clientes = (ArrayList<Cliente>) query.list();
        return clientes;

    }


    public List<Proceso> getListaProcesos(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Proceso ");
        ArrayList<Proceso> procesos = (ArrayList<Proceso>) query.list();
        return procesos;
    }

    public List<Producto> getListaProductos() {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Producto ");
        ArrayList<Producto> productos = (ArrayList<Producto>) query.list();
        return productos;
    }

    public List<Pedido> getListaPedidos(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Pedido ");
        ArrayList<Pedido> pedidos = (ArrayList<Pedido>) query.list();
        return pedidos;
    }

    public List<Albaran> getPresupuesto(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Albaran");
        ArrayList<Albaran> albaranes = (ArrayList<Albaran>) query.list();
        return albaranes;
    }

    public List<Usuario> getListaNoAdmin() {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuario WHERE tipo != 'ADMINISTRADOR'");
        ArrayList<Usuario> usuarios = (ArrayList<Usuario>) query.list();
        return usuarios;
    }

    public List<Usuario> getListaSoloConsulta() {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuario WHERE tipo LIKE 'CONSULTA'");
        ArrayList<Usuario> usuarios = (ArrayList<Usuario>) query.list();
        return usuarios;
    }



    public List<DetallePedido> getDetallePedido(int id) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM DetallePedido  WHERE id_pedido =:id");
        query.setParameter("id" , id);
        ArrayList<DetallePedido> detallePedido = (ArrayList<DetallePedido>) query.list();
        return detallePedido;
    }

    public DetallePedido getDetallePedidoEliminar(int id ) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM DetallePedido WHERE id =:id");
        query.setParameter("id",id);
        DetallePedido detallePedido = (DetallePedido) query.uniqueResult();
        return detallePedido;
    }

    public DetalleProceso getDetalleProcesoEliminar(int id ) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM DetalleProceso WHERE id =:id");
        query.setParameter("id",id);
        DetalleProceso detalleProceso = (DetalleProceso) query.uniqueResult();
        return detalleProceso;
    }


    public Usuario comprobarUsuario(String tfnick){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuario WHERE nick=:tfnick");
        query.setParameter("tfnick",tfnick);
        Usuario usuario = (Usuario) query.uniqueResult();
        return usuario;
    }

    public Usuario comprobarUsuarioContrasena(String tfpass){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuario WHERE password=:tfpass");
        query.setParameter("tfpass",tfpass);
        Usuario usuario = (Usuario) query.uniqueResult();
        return usuario;
    }

    public Usuario getUsuario(String email){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuario WHERE email =:email");
        query.setParameter("email" , email);
        Usuario usuario = (Usuario) query.uniqueResult();
        return usuario;
    }





    public Proceso getProceso(String codigo){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Proceso WHERE codigo =:codigo");
        query.setParameter("codigo",codigo);
        Proceso proceso = (Proceso) query.uniqueResult();
        return proceso;
    }

    public Producto getProducto(String codigo){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Producto WHERE codigo =:codigo");
        query.setParameter("codigo", codigo);
        Producto producto = (Producto) query.uniqueResult();
        return producto;
    }

    public Pedido getPedido(String codigo){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Pedido WHERE codigo =:codigo");
        query.setParameter("codigo",codigo);
        Pedido pedido = (Pedido) query.uniqueResult();
        return pedido;
    }

    public Albaran getPresupuesto(int numpresupuesto){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Albaran WHERE numpresupuesto =:numpresupuesto");
        query.setParameter("numpresupuesto", numpresupuesto);
        Albaran albaranPresupuesto = (Albaran) query.uniqueResult();
        return albaranPresupuesto;
    }



    public Cliente comprobarCliente(String dni){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Cliente WHERE dni=:tfdni");
        query.setParameter("tfdni",dni);
        Cliente cliente = (Cliente) query.uniqueResult();
        return cliente;
    }

    public Cliente comprobarClienteCodigo(String codigo) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Cliente WHERE codigo=:tfcodigo");
        query.setParameter("tfcodigo",codigo);
        Cliente cliente = (Cliente) query.uniqueResult();
        return cliente;
    }

    public Cliente comprobarClienteCIF(String cif,String tipo) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Cliente WHERE cif=:tfcif and tipo=:tipo");
        query.setParameter("tfcif",cif);
        query.setParameter("tipo",tipo);
        Cliente cliente = (Cliente) query.uniqueResult();
        return cliente;
    }





    public void guardardetallePedido(Object object, ArrayList<DetallePedido> listaDetalles){

        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.update(object);
        for(DetallePedido detallePedido : listaDetalles){
            session.save(detallePedido);

        }
        session.getTransaction().commit();
        session.close();

    }

    public void guardarDetalleProceso(Object object, ArrayList<DetalleProceso> listaDetalles){

        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.update(object);
        for(DetalleProceso detalleProceso : listaDetalles){
            session.save(detalleProceso);

        }
        session.getTransaction().commit();
        session.close();

    }

    public void guardarHibernate(Object object){
        Session session = HibernateUtil.getCurrentSession();

        session.beginTransaction();
        session.save(object);
        session.getTransaction().commit();

        session.close();
    }

    public void modificarHibernate(Object object){
        Session session = HibernateUtil.getCurrentSession();

        session.beginTransaction();
        session.update(object);
        session.getTransaction().commit();
        session.close();

    }

    public void eliminarHibernate(Object object){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.delete(object);
        session.getTransaction().commit();
        session.close();
    }


    public List<DetalleProceso> getDetalleProcesoProducto(int id) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM DetalleProceso WHERE id_producto =:id");
        query.setParameter("id" , id);
        ArrayList<DetalleProceso> detalleProcesos = (ArrayList<DetalleProceso>) query.list();
        return detalleProcesos;

    }


    public List<Cliente> getListaTipoClientes(String tipo){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Cliente WHERE tipo =:tipo");
        query.setParameter("tipo", tipo);
        ArrayList<Cliente> clientes = (ArrayList<Cliente>) query.list();
        return clientes;

    }

    public List<Pedido> getListaTipoPedido(String estado){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Pedido WHERE estado =:estado");
        query.setParameter("estado", estado);
        ArrayList<Pedido> pedidos = (ArrayList<Pedido>) query.list();
        return pedidos;

    }

    public List<Cliente> buscarClientesNombre(String buscar){
        List <Cliente> clientes = HibernateUtil.getCurrentSession().createCriteria(Cliente.class)
                .add(Restrictions.like("nombre",buscar+"%")).list();
        return clientes;
    }

    public List<Cliente> buscarClientesEmail(String buscar){
        List <Cliente> clientes = HibernateUtil.getCurrentSession().createCriteria(Cliente.class)
                .add(Restrictions.like("email",buscar+"%")).list();
        return clientes;
    }

    public List<Cliente> buscarClientesCodigo(String buscar){
        List <Cliente> clientes = HibernateUtil.getCurrentSession().createCriteria(Cliente.class)
                .add(Restrictions.like("codigo",buscar+"%")).list();
        return clientes;
    }

    public List<Pedido> buscarPedidoNombre(String buscar) {
        List <Pedido> pedidos = HibernateUtil.getCurrentSession().createCriteria(Pedido.class)
                .add(Restrictions.like("nombre",buscar+"%")).list();
        return pedidos;
    }

    public List<Pedido> buscarPedidoCodigo(String buscar) {
        List <Pedido> pedidos = HibernateUtil.getCurrentSession().createCriteria(Pedido.class)
                .add(Restrictions.like("codigo",buscar+"%")).list();
        return pedidos;
    }

    public List<Pedido> buscarPedidoReferencia(String buscar) {
        List <Pedido> pedidos = HibernateUtil.getCurrentSession().createCriteria(Pedido.class)
                .add(Restrictions.like("referencia",buscar+"%")).list();
        return pedidos;
    }

    public List<Cliente> buscarPedidoCliente(String buscar) {
        List <Cliente> clientes = HibernateUtil.getCurrentSession().createCriteria(Cliente.class)
                .add(Restrictions.like("nombre",buscar+"%")).list();
        return clientes;
    }

    public List<DetalleProceso> buscarDetalleProceso(String nombre){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM DetalleProceso dp WHERE dp.proceso.nombre =:nombre");
        query.setParameter("nombre" , nombre);
        List<DetalleProceso> detalleProceso = (ArrayList<DetalleProceso>) query.list();
        return  detalleProceso;
    }


    public List<DetallePedido> buscarDetallePedido(String denominacion){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM DetallePedido dp WHERE dp.producto.denominacion =:denominacion");
        query.setParameter("denominacion" , denominacion);
        List<DetallePedido> detallePedidos = (ArrayList<DetallePedido>) query.list();
        return  detallePedidos;
    }


    public List<Pedido> buscarEntreFechas(Date fechainicio,Date fechafin){
        List <Pedido> pedidos = HibernateUtil.getCurrentSession().createCriteria(Pedido.class)
                .add(Restrictions.between("fecha_inicio",fechainicio,fechafin)).list();
        return pedidos;


    }

    public List<Pedido> buscardesdeFecha(java.sql.Date fechainicio){
        Date now = new Date();
        List <Pedido> pedidos = HibernateUtil.getCurrentSession().createCriteria(Pedido.class)
                .add(Restrictions.between("fecha_inicio",fechainicio,now)).list();
        return pedidos;


    }

    public List<Proceso> buscarProcesoCodigo(String buscar) {
        List <Proceso> procesos = HibernateUtil.getCurrentSession().createCriteria(Proceso.class)
                .add(Restrictions.like("codigo",buscar+"%")).list();
        return procesos;
    }

    public List<Proceso> buscarProcesoNombre(String buscar) {
        List <Proceso> procesos = HibernateUtil.getCurrentSession().createCriteria(Proceso.class)
                .add(Restrictions.like("nombre",buscar+"%")).list();
        return procesos;
    }



    public List<Proceso> buscarProcesoPVPminimo(float pvp_minimo) {
        Query query =  HibernateUtil.getCurrentSession().createQuery("FROM Proceso p WHERE p.pvp_minimo =:pvp_minimo");
        query.setParameter("pvp_minimo" , pvp_minimo);
        List <Proceso> procesos = (ArrayList<Proceso>) query.list();
        return procesos;
    }

    public List<Proceso> buscarProcesoPVPhora(float pvp_hora) {
        Query query =  HibernateUtil.getCurrentSession().createQuery("FROM Proceso p WHERE p.pvp_hora >pvp_hora or p.pvp_hora =:pvp_hora");
        query.setParameter("pvp_hora" , pvp_hora);
        List <Proceso> procesos = (ArrayList<Proceso>) query.list();
        return procesos;
    }

    public List<Producto> buscarProductoporPrecio(float importemin) {
       Query query = HibernateUtil.getCurrentSession().createQuery("FROM Producto WHERE importe =:importemin");
       query.setParameter("importemin", importemin);
       List<Producto> productos = (ArrayList<Producto>)query.list();
       return productos;

    }


    public List<Producto> buscarProductoDenominacion(String buscar) {
        List <Producto> productos = HibernateUtil.getCurrentSession().createCriteria(Producto.class)
                .add(Restrictions.like("denominacion",buscar+"%")).list();
        return productos;
    }

    public List<Albaran> buscarPresupuesto(String nombre) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Albaran a WHERE a.pedido.cliente.nombre =:nombre");
        query.setParameter("nombre",nombre);
        List<Albaran> albaranes = (ArrayList<Albaran>)query.list();
        return albaranes;
    }

    public List<Albaran> buscarEntreFechasAlbaran(Date fechainicio,Date fechafin){
        List <Albaran> albaranes = HibernateUtil.getCurrentSession().createCriteria(Albaran.class)
                .add(Restrictions.between("fecha",fechainicio,fechafin)).list();
        return albaranes;


    }

    public List<Albaran> buscardesdeFechaAlbaran(java.sql.Date fechainicio){
        Date now = new Date();
        List <Albaran> albaranes = HibernateUtil.getCurrentSession().createCriteria(Albaran.class)
                .add(Restrictions.between("fecha",fechainicio,now)).list();
        return albaranes;


    }

    public List<Producto> buscarProductoCodigo(String buscar) {
        List <Producto> productos = HibernateUtil.getCurrentSession().createCriteria(Producto.class)
                .add(Restrictions.like("codigo",buscar+"%")).list();
        return productos;
    }

    public List<Producto> buscarProductoReferencia(String buscar) {
        List <Producto> productos = HibernateUtil.getCurrentSession().createCriteria(Producto.class)
                .add(Restrictions.like("referencia",buscar+"%")).list();
        return productos;
    }



}
