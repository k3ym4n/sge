package dvd.sge.microarte.Controller;

import dvd.sge.microarte.face.Principal;
import dvd.sge.microarte.face.VLogin;
import dvd.sge.microarte.util.Hash;
import dvd.sge.microarte.util.Util;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControllerLogin implements ActionListener {

    public static VLogin login;
    private Model model;



    public ControllerLogin(VLogin vLogin, Model model) {
        this.login = vLogin;
        this.model = model;

        listenerBotones();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==login.btacceder){

            String nick = login.tfnick.getText();
            String pass = Hash.sha1(String.valueOf(login.pfcontrasena.getPassword()));

            if(model.logeandose(nick,pass) == null){
                Util.loginerror();
                limpiarCampos();
            }else{


                try {
                    new Principal();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

                login.frame.setVisible(false);
            }

        }else if(e.getSource()==login.btcancelar){
            limpiarCampos();
        }
    }

    private void listenerBotones(){
        login.btacceder.addActionListener(this);
        login.btcancelar.addActionListener(this);
    }

    private void limpiarCampos(){
        login.tfnick.setText("");
        login.pfcontrasena.setText("");
    }
}
