package dvd.sge.microarte.Controller;



import dvd.sge.microarte.face.*;
import dvd.sge.microarte.util.TipoUsuario;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;



public class ControllerPrincipal {

    private Principal principal;
    private Model model;
    public Registro vregistro;
    public VCliente vCliente;
    public vProceso vProceso;
    public vProducto vProducto;
    public vPedido vPedido;
    private vAlbaran vAlbaran;





    private String nickusuario = ControllerLogin.login.tfnick.getText();


    public ControllerPrincipal(Principal principal, Model model) {
        this.principal = principal;
        this.model = model;

        Usuarios();
        Clientes();
        Pedidos();
        Productos();
        Procesos();
        Presupuestos();

        NivelAccesoUsuario(model.comprobarUsuario(nickusuario).getTipoUsuario());
    }


    private void Usuarios(){
       principal.lbaddUsuario.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                principal.lbaddUsuario.setIcon(new ImageIcon(getClass().getResource("/img/addUser100Black.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                principal.lbaddUsuario.setIcon(new ImageIcon(getClass().getResource("/img/addUser100.png")));
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                 new Registro();
            }
        });
    }

    private void Clientes(){
        principal.lbaddCliente.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                principal.lbaddCliente.setIcon(new ImageIcon(getClass().getResource("/img/addCliente100Black.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                principal.lbaddCliente.setIcon(new ImageIcon(getClass().getResource("/img/addCliente100.png")));
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                 new VCliente();
            }
        });
    }

    private void Pedidos(){
        principal.lbaddPedido.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                principal.lbaddPedido.setIcon(new ImageIcon(getClass().getResource("/img/addPedido100Black.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                principal.lbaddPedido.setIcon(new ImageIcon(getClass().getResource("/img/addPedido100.png")));
            }
            @Override
            public void mouseClicked(MouseEvent e) {
                new vPedido();
            }
        });
    }

    private void Productos(){
        principal.lbaddProducto.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                principal.lbaddProducto.setIcon(new ImageIcon(getClass().getResource("/img/addProducto100Black.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                principal.lbaddProducto.setIcon(new ImageIcon(getClass().getResource("/img/addProducto100.png")));
            }

            @Override
            public void mouseClicked(MouseEvent e) {

                    new vProducto();
            }
        });
    }


    private void Procesos(){
        principal.lbaddProceso.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                principal.lbaddProceso.setIcon(new ImageIcon(getClass().getResource("/img/addProceso100Black.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                principal.lbaddProceso.setIcon(new ImageIcon(getClass().getResource("/img/addProceso100.png")));
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                new vProceso();
            }
        });
    }

    private void Presupuestos(){
        principal.lbaddFactura.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                principal.lbaddFactura.setIcon(new ImageIcon(getClass().getResource("/img/addFactura100Black.png")));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                principal.lbaddFactura.setIcon(new ImageIcon(getClass().getResource("/img/addFactura100.png")));
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                    new vAlbaran();
            }
        });
    }






    private void NivelAccesoUsuario(TipoUsuario tipoUsuario){
        switch (tipoUsuario) {
            case CONSULTA:

                break;
        }

    }
}
