package dvd.sge.microarte.Controller;

import dvd.sge.microarte.base.Cliente;
import dvd.sge.microarte.face.Botonera;
import dvd.sge.microarte.face.VCliente;
import dvd.sge.microarte.util.Conexion;
import dvd.sge.microarte.util.TipoCliente;
import dvd.sge.microarte.util.TipoUsuario;
import dvd.sge.microarte.util.Util;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.io.File;
import java.sql.Connection;
import java.util.List;

public class ControllerCliente implements ActionListener {

    private VCliente vCliente;
    private Model model;
    private Botonera botonera;

    private Cliente cliente;
    public TipoCliente tipoCliente;

    private DefaultTableModel dtmCliente;

    private String nickusuario = ControllerLogin.login.tfnick.getText();


    public ControllerCliente(VCliente vCliente, Model model, Botonera botonera) {
        this.vCliente = vCliente;
        this.model = model;
        this.botonera = botonera;

        listenerbotones();
        modeloTablaClientes();
        cbTipoUsuario();
        listarTablaClientes();
        listenerTablaVClientes();
        vCliente.lbnick.setText(nickusuario);
        nivelAccesoUsuario(model.comprobarUsuario(nickusuario).getTipoUsuario());

        buscarporNombre();
        buscarCodigo();
        buscarEmail();
        buscarTipo();

        botonera.btmodificar.setEnabled(false);
        botonera.bteliminar.setEnabled(false);

    }

    private void listenerbotones() {
        botonera.btguardar.addActionListener(this);
        botonera.btmodificar.addActionListener(this);
        botonera.btcancelar.addActionListener(this);
        botonera.bteliminar.addActionListener(this);
        vCliente.btinformeClientes.addActionListener(this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == botonera.btguardar) {

            if (comprobarCamposGuardar() == true) {

                guardarCliente();
                vCliente.lbinfo.setText("Cliente guardado correctamente");

            }
            limpiarCampos();
            listarTablaClientes();
            cbTipoUsuario();

        } else if (e.getSource() == botonera.btmodificar) {
            if(comprobarCamposGuardar() == true){
            int filaCliente = vCliente.tablaClientes.getSelectedRow();
            String dni = (String) vCliente.tablaClientes.getValueAt(filaCliente,10);
            cliente = model.comprobarCliente(dni);

                modificarCliente(cliente);
                limpiarCampos();
                listarTablaClientes();
                cbTipoUsuario();
                botonera.btmodificar.setEnabled(false);
                botonera.bteliminar.setEnabled(false);

            }
            botonera.btguardar.setEnabled(true);
            botonera.btmodificar.setEnabled(false);

        } else if (e.getSource() == botonera.btcancelar) {
            botonera.btguardar.setEnabled(true);
            limpiarCampos();
            cbTipoUsuario();
            botonera.btmodificar.setEnabled(false);
            botonera.bteliminar.setEnabled(false);

        } else if (e.getSource() == botonera.bteliminar) {

            if(comprobarCamposGuardar() == true){
                int filaCliente = vCliente.tablaClientes.getSelectedRow();
                String codigo = (String) vCliente.tablaClientes.getValueAt(filaCliente, 0);
                cliente = model.comprobarClienteCodigo(codigo);
                model.eliminarHibernate(cliente);
                listarTablaClientes();
                limpiarCampos();
                cbTipoUsuario();
                botonera.btguardar.setEnabled(true);
                botonera.btmodificar.setEnabled(false);
                botonera.bteliminar.setEnabled(false);
            }

        }else if(e.getSource() == vCliente.btinformeClientes){

            informeClientes();
        }

    }

    private void informeClientes(){
        Conexion conexion = new Conexion();
        Connection connection = conexion.getConexion();
        String path = "src\\reportes\\clientes\\Clientes.jasper";
        JasperReport report = null;
        try {


            report = (JasperReport) JRLoader.loadObjectFromFile(String.valueOf(new File(path)));

            JasperPrint print = JasperFillManager.fillReport(path,  null,connection);
            JasperViewer view = new JasperViewer(print,false);

            view.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            view.setVisible(true);
        } catch (JRException e2) {
            JOptionPane.showMessageDialog(null , "No se ha podido mostrar el informe");
        }

    }


    private void guardarCliente() {
        cliente = new Cliente();
        cliente.setNombre(vCliente.tfnombre.getText());
        cliente.setCif(vCliente.tfcif.getText());
        cliente.setDni(vCliente.tfdni.getText());
        cliente.setDireccion(vCliente.tfdireccion.getText());
        cliente.setCP(vCliente.tfCP.getText());
        cliente.setProvincia(vCliente.tfprovincia.getText());
        cliente.setEmail(vCliente.tfemail.getText());
        cliente.setTelefono_contacto(vCliente.tftelefono1.getText());
        cliente.setTelefono_contacto2(vCliente.tftelefono2.getText());
        cliente.setCodigo(vCliente.tfcodigo.getText());
        cliente.setTipoCliente((TipoCliente) vCliente.cbtipoCliente.getSelectedItem());
        cliente.setUsuario(model.comprobarUsuario(vCliente.lbnick.getText()));


        model.guardarHibernate(cliente);
        limpiarCampos();

    }

    private void modificarCliente(Cliente cliente) {
        cliente.setNombre(vCliente.tfnombre.getText());
        cliente.setDireccion(vCliente.tfdireccion.getText());
        cliente.setCP(vCliente.tfCP.getText());
        cliente.setProvincia(vCliente.tfprovincia.getText());
        cliente.setEmail(vCliente.tfemail.getText());
        cliente.setTelefono_contacto(vCliente.tftelefono1.getText());
        cliente.setTelefono_contacto2(vCliente.tftelefono2.getText());
        cliente.setTipoCliente((TipoCliente) vCliente.cbtipoCliente.getSelectedItem());

        TipoCliente tipoCliente = (TipoCliente) vCliente.cbtipoCliente.getSelectedItem();

        switch (tipoCliente){
            case EMPRESA:

               modificarComprobacionCliente(cliente);


                if(vCliente.tfcif.getText().isEmpty()){
                    String cif = cliente.getCif();
                    cliente.setCif(cif);
                    model.modificarHibernate(cliente);
                }else if(model.comprobarClienteCIF(vCliente.tfcif.getText(),String.valueOf(tipoCliente)) != null){
                    String cif = cliente.getCif();
                    cliente.setCif(cif);
                    JOptionPane.showMessageDialog(null , "El C.I.F ya existe");
                }else{
                    cliente.setCif(vCliente.tfcif.getText());
                    model.modificarHibernate(cliente);
                }
                break;
            case PARTICULAR:
                modificarComprobacionCliente(cliente);
                break;
        }

    }

    private void modificarComprobacionCliente(Cliente cliente){

        if(vCliente.tfcodigo.getText().isEmpty()){
            String codigo = cliente.getCodigo();
            cliente.setCodigo(codigo);
        }else if(model.comprobarClienteCodigo(vCliente.tfcodigo.getText()) != null){
            String codigo = cliente.getCodigo();
            cliente.setCodigo(codigo);
            JOptionPane.showMessageDialog(null,"El Codigo ya existe o no has seleccionado ningun Cliente de la Tabla");
        }else{
            cliente.setCodigo(vCliente.tfcodigo.getText());
        }

        if(vCliente.tfdni.getText().isEmpty()){
            String dni = cliente.getDni();
            cliente.setDni(dni);
            model.modificarHibernate(cliente);
        }else if(model.comprobarCliente(vCliente.tfdni.getText()) != null){
            String dni = cliente.getDni();
            cliente.setDni(dni);
            JOptionPane.showMessageDialog(null,"El D.N.I ya existe");
        }else{
            cliente.setDni(vCliente.tfdni.getText());
            model.modificarHibernate(cliente);
        }
    }

    private void limpiarCampos() {
        vCliente.tfnombre.setText("");
        vCliente.tfcif.setText("");
        vCliente.tfdni.setText("");
        vCliente.tfdireccion.setText("");
        vCliente.tfCP.setText("");
        vCliente.tfprovincia.setText("");
        vCliente.tfemail.setText("");
        vCliente.tftelefono1.setText("");
        vCliente.tftelefono2.setText("");
        vCliente.tfcodigo.setText("");
        vCliente.cbtipoCliente.setSelectedItem("Tipo de Cliente");
        vCliente.lbinfo.setText("");


    }


    private void listenerTablaVClientes() {

        vCliente.tablaClientes.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Cliente cliente = model.comprobarClienteCodigo((String) vCliente.tablaClientes.getValueAt(vCliente.tablaClientes.getSelectedRow(), 0));
                limpiarCampos();
                vCliente.tfnombre.setText(cliente.getNombre());
                vCliente.tfdireccion.setText(cliente.getDireccion());
                vCliente.tfCP.setText(cliente.getCP());
                vCliente.tfprovincia.setText(cliente.getProvincia());
                vCliente.tfemail.setText(cliente.getEmail());
                vCliente.tftelefono1.setText(cliente.getTelefono_contacto());
                vCliente.tftelefono2.setText(cliente.getTelefono_contacto2());
                vCliente.cbtipoCliente.setSelectedItem(cliente.getTipoCliente());


                botonera.btguardar.setEnabled(false);
                botonera.btmodificar.setEnabled(true);
                botonera.bteliminar.setEnabled(true);

            }
        });


    }

    private void modeloTablaClientes() {

        dtmCliente = new DefaultTableModel();
        dtmCliente.addColumn("Codigo");
        dtmCliente.addColumn("Nombre");
        dtmCliente.addColumn("Direccion");
        dtmCliente.addColumn("C.P");
        dtmCliente.addColumn("Provincia");
        dtmCliente.addColumn("EMail");
        dtmCliente.addColumn("Telefono 1");
        dtmCliente.addColumn("Telefono 2");
        dtmCliente.addColumn("Tipo Cliente");
        dtmCliente.addColumn("C.I.F");
        dtmCliente.addColumn("D.N.I");

        vCliente.tablaClientes.setModel(dtmCliente);

    }

    private void listarTablaClientes() {
        List<Cliente> listaClientes = model.getListaClientes();
        if (listaClientes != null) {
            dtmCliente.setRowCount(0);
            for (Cliente cliente : listaClientes) {
                Object[] filaCliente = new Object[]{cliente.getCodigo(), cliente.getNombre(), cliente.getDireccion(), cliente.getCP(), cliente.getProvincia(),
                        cliente.getEmail(), cliente.getTelefono_contacto(), cliente.getTelefono_contacto2(), cliente.getTipoCliente(),
                        cliente.getCif(), cliente.getDni()};
                dtmCliente.addRow(filaCliente);
            }


        } else {
            vCliente.lbinfo.setText("No hay datos para listar en la tabla de clientes");
        }
    }



    private void cbTipoUsuario() {
        vCliente.cbtipoCliente.removeAllItems();
        vCliente.cbbuscarTipo.removeAllItems();
        vCliente.cbtipoCliente.addItem("TIPO DE CLIENTE");
        vCliente.cbbuscarTipo.addItem("TIPO CLIENTE");
        TipoCliente[] tipoClientes = TipoCliente.values();
        for (TipoCliente tipoCliente : tipoClientes) {
            vCliente.cbtipoCliente.addItem(tipoCliente);
            vCliente.cbbuscarTipo.addItem(tipoCliente);
        }
    }

    private boolean comprobarCamposGuardar(){

        if(vCliente.cbtipoCliente.getSelectedIndex() <= 0){
            JOptionPane.showMessageDialog(null, "Debes escoger un Tipo de Cliente (PARTICULAR o EMPRESA)");
            return false;
        }else{
            tipoCliente = (TipoCliente) vCliente.cbtipoCliente.getSelectedItem();
        }

        if (model.comprobarClienteCodigo(vCliente.tfcodigo.getText()) != null || vCliente.tfcodigo.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "El campo Codigo ya existe o esta vacio");
            return false;
        }


        switch(tipoCliente){

            case PARTICULAR:
                if(comprobarCampos()==false){
                    return false;
                }else if(vCliente.tfcif.getText().isEmpty()){
                    return true;
                }

                break;

            case EMPRESA:
                if(comprobarCampos() == false){
                    return false;
                    }else{
                    if (model.comprobarClienteCIF(vCliente.tfcif.getText(), String.valueOf(tipoCliente)) != null || vCliente.tfcif.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(null, "El C.I.F ya existe o esta vacio");
                        return false;
                    }else if (model.comprobarClienteCodigo(vCliente.tfcodigo.getText()) != null || vCliente.tfcodigo.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(null, "El campo Codigo ya existe o esta vacio");
                        return false;
                    }else if (model.comprobarCliente(vCliente.tfdni.getText()) != null || vCliente.tfdni.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(null, "El usuario ya existe");
                        return false;
                    }
                }
        }

        return true;

    }

    private boolean comprobarCampos() {


        if (vCliente.tfnombre.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "El campo Nombre esta vacio");
            return false;
        } else if (vCliente.tfdireccion.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "El campo Direccion esta vacio");
            return false;
        } else if (vCliente.tfCP.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "El campo C.P esta vacio");
            return false;
        } else if (vCliente.tfprovincia.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "El campo Provincia esta vacio");
            return false;
        } else if (Util.esNumero(vCliente.tftelefono1.getText()) == false || vCliente.tftelefono1.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "El campo Telefono 1 solo admite numeros o esta vacio");
            return false;
        } else if (Util.esNumero(vCliente.tftelefono2.getText()) == false || vCliente.tftelefono2.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "El campo Telefono 2 solo admite numeros o esta vacio");
            return false;
        } else if (Util.esEmail(vCliente.tfemail.getText()) == false || vCliente.tfemail.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "El campo EMAIL tiene un formato incorrecto recuerde xxxx@xxxx.xxx o esta vacio");
            return false;
        }  else if (vCliente.cbtipoCliente.getSelectedIndex() <= 0  ) {
            JOptionPane.showMessageDialog(null, "Debes escoger un Tipo de Cliente (PARTICULAR o EMPRESA)");
            return false;
        }

        return  true;
    }



    private void nivelAccesoUsuario(TipoUsuario tipoUsuario) {

        switch (tipoUsuario) {
            case CONSULTA:
                vCliente.panelBotonera.setVisible(false);
                vCliente.menuBar.setEnabled(false);

                break;
        }

    }

    private void buscarporNombre(){

        vCliente.tfbuscarNombre.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vCliente.tfbuscarNombre.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vCliente.tfbuscarNombre.setText("Nombre...");
                listarTablaClientes();

            }
        });

        vCliente.tfbuscarNombre.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {


            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = vCliente.tfbuscarNombre.getText();
                List<Cliente> listaClientesNombre = model.buscarClientesNombre(texto);
                dtmCliente.setRowCount(0);
                for (Cliente cliente : listaClientesNombre) {
                    Object[] filaClienteNombre = new Object[]{cliente.getCodigo(), cliente.getNombre(), cliente.getDireccion(), cliente.getCP(), cliente.getProvincia(),
                            cliente.getEmail(), cliente.getTelefono_contacto(), cliente.getTelefono_contacto2(), cliente.getTipoCliente(),
                            cliente.getCif(), cliente.getDni()};
                    dtmCliente.addRow(filaClienteNombre);
                }

            }
        });
    }


    private void buscarEmail(){

        vCliente.tfbuscarEmail.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vCliente.tfbuscarEmail.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vCliente.tfbuscarEmail.setText("@Email...");
                listarTablaClientes();
            }
        });

        vCliente.tfbuscarEmail.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = vCliente.tfbuscarEmail.getText();
                List<Cliente> listaClientesEmail = model.buscarClientesEmail(texto);
                dtmCliente.setRowCount(0);
                for (Cliente cliente : listaClientesEmail) {
                    Object[] filaClienteEmail = new Object[]{cliente.getCodigo(), cliente.getNombre(), cliente.getDireccion(), cliente.getCP(), cliente.getProvincia(),
                            cliente.getEmail(), cliente.getTelefono_contacto(), cliente.getTelefono_contacto2(), cliente.getTipoCliente(),
                            cliente.getCif(), cliente.getDni()};
                    dtmCliente.addRow(filaClienteEmail);
                }
            }
        });
    }

    private void buscarCodigo(){

        vCliente.tfbuscarCodigo.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                vCliente.tfbuscarCodigo.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                vCliente.tfbuscarCodigo.setText("Codigo...");
                listarTablaClientes();
            }
        });
        vCliente.tfbuscarCodigo.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = vCliente.tfbuscarCodigo.getText();
                List<Cliente> listaClientesCodigo = model.buscarClientesCodigo(texto);
                dtmCliente.setRowCount(0);
                for (Cliente cliente : listaClientesCodigo) {
                    Object[] filaClienteCodigo = new Object[]{cliente.getCodigo(), cliente.getNombre(), cliente.getDireccion(), cliente.getCP(), cliente.getProvincia(),
                            cliente.getEmail(), cliente.getTelefono_contacto(), cliente.getTelefono_contacto2(), cliente.getTipoCliente(),
                            cliente.getCif(), cliente.getDni()};
                    dtmCliente.addRow(filaClienteCodigo);
                }
            }
        });
    }


    private void buscarTipo(){
        /**Para escuchar los cambios de los elementos seleccionados en el ComboBox,
         *  necesitamos agregar un ItemListener al componente del ComboBox */

        vCliente.cbbuscarTipo.addItemListener(new ItemListener() {

            /**Escucha si se han seleccionado nuevos elementos del cuadro combinado*/
            @Override
            public void itemStateChanged(ItemEvent e) {

                vCliente.cbbuscarTipo = (JComboBox) e.getSource();

                /**El elemento afectado por el evento*/
                Object item = e.getItem();

                /**Y la accion sobre el elemento que se selecciona, en este caso buscar segun el tipo de cliente*/
                if (e.getStateChange() == ItemEvent.SELECTED) {

                    if(vCliente.cbbuscarTipo.getSelectedIndex()<= 0){
                        listarTablaClientes();
                    }else{
                        vCliente.lbinfo.setText(item.toString() + " Seleccionado.");
                        String texto = item.toString();
                        List<Cliente> listaClientesTipo = model.getListaTipoClientes(texto);
                        dtmCliente.setRowCount(0);
                        for (Cliente cliente : listaClientesTipo) {
                            Object[] filaClienteTipo = new Object[]{cliente.getCodigo(), cliente.getNombre(), cliente.getDireccion(), cliente.getCP(), cliente.getProvincia(),
                                    cliente.getEmail(), cliente.getTelefono_contacto(), cliente.getTelefono_contacto2(), cliente.getTipoCliente(),
                                    cliente.getCif(), cliente.getDni()};
                            dtmCliente.addRow(filaClienteTipo);
                        }
                    }


                }

            }
        });





    }

}
