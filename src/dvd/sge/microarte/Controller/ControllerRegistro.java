package dvd.sge.microarte.Controller;

import dvd.sge.microarte.base.Usuario;
import dvd.sge.microarte.face.Botonera;
import dvd.sge.microarte.face.Registro;
import dvd.sge.microarte.util.Conexion;
import dvd.sge.microarte.util.Hash;
import dvd.sge.microarte.util.TipoUsuario;
import dvd.sge.microarte.util.Util;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;


import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.Connection;
import java.util.*;

import static java.lang.String.valueOf;

public class ControllerRegistro implements ActionListener {

    private Registro registro;
    private Model model;
    private Botonera botonera;

    private DefaultTableModel dtmRegistro;
    private Usuario usuario;


    private String nickusuario = ControllerLogin.login.tfnick.getText();


    public ControllerRegistro(Registro registro, Model model, Botonera botonera ) {

        this.registro = registro;
        this.model = model;
        this.botonera = botonera;


         listenerBotonera();
         rellenarCBregistro();
         modeloTablaRegistro();
         listarTablaRegistro(model.comprobarUsuario(nickusuario).getTipoUsuario());
         listenerTabla();
         registro.lbnick.setText(nickusuario);
         nivelAccesoUsuario(model.comprobarUsuario(nickusuario).getTipoUsuario());

         botonera.btmodificar.setEnabled(false);
         botonera.bteliminar.setEnabled(false);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource()==botonera.btguardar){

                if(comprobarCamposGuardar()==true){
                    guardarUsuario();
                    limpiarCampos();
                    listarTablaRegistro(model.comprobarUsuario(nickusuario).getTipoUsuario());
                    registro.lbinfo.setText("Usuario guardado correctamente");
                }
                limpiarCampos();

        }else if(e.getSource()==botonera.btmodificar){
            if(comprobarCamposModificar() == true){
                try{
                    int filausuario = registro.tablausuarios.getSelectedRow();
                    String email= (String) registro.tablausuarios.getValueAt(filausuario,3);
                    usuario = model.getUsuario(email);
                    modificarUsuario(usuario);
                }catch (ArrayIndexOutOfBoundsException aiobe){
                    registro.lbinfo.setText("Debes escoger un usuario de la tabla Usuarios");
                }

                    listarTablaRegistro(model.comprobarUsuario(nickusuario).getTipoUsuario());
                    limpiarCampos();
                    botonera.btguardar.setEnabled(true);
                    botonera.btmodificar.setEnabled(false);
                    botonera.bteliminar.setEnabled(false);

                }



        }else if(e.getSource()==botonera.btcancelar){
            botonera.btguardar.setEnabled(true);
            botonera.btmodificar.setEnabled(false);
            botonera.bteliminar.setEnabled(false);
            limpiarCampos();


        }else if(e.getSource()==botonera.bteliminar){
            if(comprobarCamposModificar() == true){
                try{
                    int filausuario = registro.tablausuarios.getSelectedRow();
                    String email= (String) registro.tablausuarios.getValueAt(filausuario,3);
                    usuario = model.getUsuario(email);
                    model.eliminarHibernate(usuario);
                }catch(ArrayIndexOutOfBoundsException aiobe){
                    registro.lbinfo.setText("Debes escoger un usuario de la tabla Usuarios");
                }

                botonera.btmodificar.setEnabled(false);
                botonera.bteliminar.setEnabled(false);
                listarTablaRegistro(model.comprobarUsuario(nickusuario).getTipoUsuario());
                limpiarCampos();
            }
        }else if(e.getSource() == registro.btinforme){
            infromeusuarios();
        }

    }

    private void infromeusuarios(){
        Conexion conexion = new Conexion();
        Connection connection = conexion.getConexion();
        String path = "src\\reportes\\usuarios\\Usuarios.jasper";
        JasperReport reporte = null;
        try {

            reporte = (JasperReport) JRLoader.loadObjectFromFile(String.valueOf(new File(path)));

            JasperPrint print = JasperFillManager.fillReport(path,  null,connection);
            JasperViewer view = new JasperViewer(print,false);

            view.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            view.setVisible(true);

        }catch (JRException eu){
            eu.printStackTrace();
        }
    }

    private boolean comprobarCamposGuardar(){

        String nombre = registro.tfnombre.getText();
        String apellidos = registro.tfapellidos.getText();
        String direccion = registro.tfdireccion.getText();
        String password = valueOf(registro.pfcontrasena.getPassword());
        String comprobarPass = valueOf(registro.pfconfirmcontrasena.getPassword());
        String comprobarEmail = registro.tfemail.getText();
        String comprobarExisteUsuario = registro.tfnick.getText();
        String telefonos1 = registro.tftelefono1.getText();
        String telefono2 = registro.tftelefono2.getText();

        if(nombre.isEmpty()){
            JOptionPane.showMessageDialog(null,"El campo Nombre esta vacio");
            return false;
        }else if(apellidos.isEmpty()){
            JOptionPane.showMessageDialog(null,"El campo Apellidos esta vacio");
            return false;
        }else if(direccion.isEmpty()){
            JOptionPane.showMessageDialog(null,"El campo Direccion esta vacio");
            return false;
        }else if(password.isEmpty()){
            JOptionPane.showMessageDialog(null,"El campo Contraseña esta vacio");
            return false;
        }else if(!password.equals(comprobarPass)){
            JOptionPane.showMessageDialog(null,"Los campos de la contraseña no coinciden ");
            limpiarCamposPassword();
            return  false;
        }else if(Util.esEmail(comprobarEmail) == false) {
            JOptionPane.showMessageDialog(null, "El campo Email no es correcto ,recuerde xxxxx@xxx.xxx");
            limpiarCampoEmail();
            return  false;
        } else if(Util.esNumero(telefonos1)==false || telefonos1.isEmpty()){
            Util.esNumeroError();
            return  false;
        }else if(Util.esNumero(telefono2)==false || telefono2.isEmpty()){
            Util.esNumeroError();
            return  false;
        }else if(model.comprobarUsuario(comprobarExisteUsuario) != null){
            JOptionPane.showMessageDialog(null,"El campo Nick "+ comprobarExisteUsuario + " no es valido ");
            return  false;
        }else if(registro.cbTipousuario.getSelectedIndex() <= 0){
            JOptionPane.showMessageDialog(null,"Debes escoger un nivel de acceso para el usuario ");
            return  false;
        }

        return true;
    }
    private boolean comprobarCamposModificar(){

        String nombre = registro.tfnombre.getText();
        String apellidos = registro.tfapellidos.getText();
        String direccion = registro.tfdireccion.getText();
        String password = valueOf(registro.pfcontrasena.getPassword());
        String comprobarPass = valueOf(registro.pfconfirmcontrasena.getPassword());
        String comprobarEmail = registro.tfemail.getText();
        String telefonos1 = registro.tftelefono1.getText();
        String telefono2 = registro.tftelefono2.getText();

        if(nombre.isEmpty()){
            JOptionPane.showMessageDialog(null,"El campo Nombre esta vacio");
        }else if(apellidos.isEmpty()){
            JOptionPane.showMessageDialog(null,"El campo Apellidos esta vacio");
        }else if(direccion.isEmpty()){
            JOptionPane.showMessageDialog(null,"El campo Direccion esta vacio");
        }else if(!password.equals(comprobarPass)){
            JOptionPane.showMessageDialog(null,"Los campos de la contraseña no coinciden ");
            limpiarCamposPassword();
            return  false;
        }else if(Util.esEmail(comprobarEmail) == false) {
            JOptionPane.showMessageDialog(null, "El campo Email no es correcto ,recuerde xxxxx@xxx.xxx");

            return  false;
        } else if(Util.esNumero(telefonos1)==false || telefonos1.isEmpty()){
            Util.esNumeroError();
            return  false;
        }else if(Util.esNumero(telefono2)==false || telefono2.isEmpty()){
            Util.esNumeroError();
            return  false;
        }else if(registro.cbTipousuario.getSelectedIndex() <= 0){
            JOptionPane.showMessageDialog(null,"Debes escoger un nivel de acceso para el usuario ");
            return  false;
        }

        return true;
    }
    private void guardarUsuario(){
        usuario = new Usuario();

        usuario.setNombre(registro.tfnombre.getText());
        usuario.setApellido(registro.tfapellidos.getText());
        usuario.setDireccion(registro.tfdireccion.getText());
        usuario.setEmail(registro.tfemail.getText());

        Date now = new Date();
        usuario.setFecha_registro(now);
        usuario.setTlf_fijo(registro.tftelefono1.getText());
        usuario.setTlf_movil(registro.tftelefono2.getText());
        usuario.setTipoUsuario((TipoUsuario) registro.cbTipousuario.getSelectedItem());
        usuario.setNick(registro.tfnick.getText());
        usuario.setPassword(Util.encriptaClaveSH1(valueOf(registro.pfcontrasena.getPassword())));

        model.guardarHibernate(usuario);
    }
    private void modificarUsuario(Usuario usuario){

        usuario.setNombre(registro.tfnombre.getText());
        usuario.setApellido(registro.tfapellidos.getText());
        usuario.setDireccion(registro.tfdireccion.getText());
        usuario.setEmail(registro.tfemail.getText());
        usuario.setTlf_fijo(registro.tftelefono1.getText());
        usuario.setTlf_movil(registro.tftelefono2.getText());
        usuario.setTipoUsuario((TipoUsuario) registro.cbTipousuario.getSelectedItem());

        String password = valueOf(registro.pfcontrasena.getPassword());

         if(registro.tfnick.getText().isEmpty() ){
            String nickuser =usuario.getNick();
            usuario.setNick(nickuser);
             model.modificarHibernate(usuario);

        }else if(model.comprobarUsuario(registro.tfnick.getText()) != null){
             String nickuser =usuario.getNick();
             usuario.setNick(nickuser);
             JOptionPane.showMessageDialog(null,"El usuario con ese nick ya existe");

        }else{
             usuario.setNick(registro.tfnick.getText());
             model.modificarHibernate(usuario);
         }

        if(password.isEmpty()){
                String passuser = usuario.getPassword();
                usuario.setPassword(passuser);
            model.modificarHibernate(usuario);
        }else if (model.comprobarUsuarioContrasena(valueOf(Util.encriptaClaveSH1(String.valueOf(registro.pfcontrasena.getPassword())))) != null){
            String passuser = usuario.getPassword();
            usuario.setPassword(passuser);
            JOptionPane.showMessageDialog(null,"La contraseña no es valida");

        }else{
            usuario.setPassword(String.valueOf(Hash.sha1(String.valueOf(registro.pfcontrasena.getPassword()))));
            model.modificarHibernate(usuario);
        }



    }
    private void listenerTabla(){
        registro.tablausuarios.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Usuario usuario = model.getUsuario((String) registro.tablausuarios.getValueAt(registro.tablausuarios.getSelectedRow(),3));
                registro.tfnombre.setText(usuario.getNombre());
                registro.tfapellidos.setText(usuario.getApellido());
                registro.tfdireccion.setText(usuario.getDireccion());
                registro.tfemail.setText(usuario.getEmail());
                registro.tftelefono1.setText(usuario.getTlf_fijo());
                registro.tftelefono2.setText(usuario.getTlf_movil());
                registro.cbTipousuario.setSelectedItem(usuario.getTipoUsuario());

                botonera.btguardar.setEnabled(false);
                botonera.btmodificar.setEnabled(true);
                botonera.bteliminar.setEnabled(true);
            }
        });
    }
    private void listenerBotonera(){
        botonera.btguardar.addActionListener(this);
        botonera.btmodificar.addActionListener(this);
        botonera.btcancelar.addActionListener(this);
        botonera.bteliminar.addActionListener(this);
        registro.btinforme.addActionListener(this);

    }
    private void limpiarCamposPassword() {

        registro.pfcontrasena.setText("");
        registro.pfconfirmcontrasena.setText("");
    }
    private void limpiarCampoEmail(){
        registro.tfemail.setText("");
    }
    private void limpiarCampos(){
        registro.tfnombre.setText("");
        registro.tfapellidos.setText("");
        registro.tfdireccion.setText("");
        registro.tfemail.setText("");
        registro.tftelefono1.setText("");
        registro.tftelefono2.setText("");
        registro.cbTipousuario.setSelectedIndex(0);
        registro.tfnick.setText("");
        registro.pfcontrasena.setText("");
        registro.pfconfirmcontrasena.setText("");
        registro.lbinfo.setText("");
    }
    private void modeloTablaRegistro(){
        dtmRegistro = new DefaultTableModel();
        dtmRegistro.addColumn("Nombre");
        dtmRegistro.addColumn("Apellido");
        dtmRegistro.addColumn("Direccion");
        dtmRegistro.addColumn("Email");
        dtmRegistro.addColumn("Telefono 1");
        dtmRegistro.addColumn("Telefono 2");
        dtmRegistro.addColumn("Tipo Usuario");

        registro.tablausuarios.setModel(dtmRegistro);
    }
    private void rellenarCBregistro(){
        registro.cbTipousuario.removeAllItems();
        registro.cbTipousuario.addItem("NIVEL ACCESO USUARIO");
        TipoUsuario [] tipo = TipoUsuario.values();
        for(TipoUsuario tipoUsuario : tipo){
            registro.cbTipousuario.addItem(tipoUsuario);
        }
    }
    private void nivelAccesoUsuario(TipoUsuario tipoUsuario){
        switch (tipoUsuario){
            case ADMINISTRADOR:
                registro.lbinfo.setText("Bienvenido " + tipoUsuario);
                listarTablaRegistro(tipoUsuario);
                break;
            case OPERARIO:
                registro.cbTipousuario.removeItem(TipoUsuario.ADMINISTRADOR);
                registro.lbinfo.setText("Bienvenido " + tipoUsuario );
                registro.tfnick.setEnabled(false);
                registro.pfcontrasena.setEnabled(false);
                registro.pfconfirmcontrasena.setEnabled(false);
                listarTablaRegistro(tipoUsuario);
                break;
            case CONSULTA:
                listarTablaRegistro(tipoUsuario);
                registro.lbinfo.setText("Bienvenido " + tipoUsuario );


                registro.panelBotonera.setVisible(false);
                registro.panelRegistro.setEnabled(false);
                registro.btinforme.setEnabled(false);
                registro.cbTipousuario.setVisible(false);




                break;
        }
    }
    private void listarTablaRegistro(TipoUsuario tipoUsuario){
        switch (tipoUsuario){
            case ADMINISTRADOR:
                List<Usuario> todosusuarios = model.getListaUsuarios();
                dtmRegistro.setNumRows(0);
                for(Usuario usuario :todosusuarios){
                    Object [] filaR = new Object[]{usuario.getNombre(), usuario.getApellido(), usuario.getDireccion(), usuario.getEmail(),
                            usuario.getTlf_fijo(), usuario.getTlf_movil(), usuario.getTipoUsuario()};
                    dtmRegistro.addRow(filaR);
                }
                break;
            case OPERARIO:
                List<Usuario> noAdmin = model.getListaNoAdmin();
                dtmRegistro.setNumRows(0);
                for(Usuario usuario :noAdmin){
                    Object [] filaR = new Object[]{usuario.getNombre(), usuario.getApellido(), usuario.getDireccion(), usuario.getEmail(),
                            usuario.getTlf_fijo(), usuario.getTlf_movil(), usuario.getTipoUsuario()};
                    dtmRegistro.addRow(filaR);
                }
                break;
            case CONSULTA:
                List<Usuario> soloOperarios = model.getListaSoloConsulta();
                dtmRegistro.setNumRows(0);
                for(Usuario usuario :soloOperarios){
                    Object [] filaR = new Object[]{usuario.getNombre(), usuario.getApellido(), usuario.getDireccion(), usuario.getEmail(),
                            usuario.getTlf_fijo(), usuario.getTlf_movil(), usuario.getTipoUsuario()};
                    dtmRegistro.addRow(filaR);
                }
                break;
        }

    }
}
