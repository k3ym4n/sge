package dvd.sge.microarte.util;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MoverVentanas {

    private  JFrame frmover;
    private JPanel panelDetalles;
    private int x ,y;
    private int xmouse,ymouse;

    public MoverVentanas(JFrame frame){
        this.frmover = frame;

        frame.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                x = e.getXOnScreen();
                y = e.getYOnScreen();
                frame.setLocation(x - xmouse,y - ymouse);


            }
        });

        frame.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                xmouse=e.getX();
                ymouse=e.getY();


            }
        });
    }



}
