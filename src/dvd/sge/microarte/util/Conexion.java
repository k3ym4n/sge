package dvd.sge.microarte.util;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

    public static final String url = "jdbc:mysql://localhost:3306/micropresupuestos";
    public static final String username ="root";
    public static final String password = "";


    public Conexion(){


    }

    public static Connection getConexion(){
        Connection conexion = null;

        try{

            Class.forName("com.mysql.jdbc.Driver");

             conexion = DriverManager.getConnection(url,username,password);
        }catch(Exception e){

            JOptionPane.showMessageDialog(null,"No se pudo conectar con la base de datos");

        }

        return  conexion;
    }

}
