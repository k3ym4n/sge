package dvd.sge.microarte.util;

public enum TipoUsuario {

    ADMINISTRADOR, OPERARIO, CONSULTA
}
