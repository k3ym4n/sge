package dvd.sge.microarte.util;

import dvd.sge.microarte.Controller.ControllerLogin;
import dvd.sge.microarte.Controller.Model;
import dvd.sge.microarte.face.*;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.swixml.SwingEngine;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Connection;

public class MenuBar extends JFrame implements ActionListener {

    public JPanel panelPrincipal;

    public JMenu minuevo;
    public JMenuItem miusuario;
    public JMenuItem micliente;
    public JMenuItem miproceso;



    public JMenu micrear;
    public JMenuItem cpedido;
    public JMenuItem cproducto;
    public JMenuItem cpresupuesto;



    public JMenu miinfo;
    public JMenuItem infousuarios;
    public JMenuItem infoclientes;
    public JMenuItem infoprocesos;
    public JMenuItem infoproductos;
    public JMenuItem infopedidos;
    public JMenuItem infopresupuestos;




    private TipoUsuario tipoUsuario;
    private Model model;

    private String usuarionick = ControllerLogin.login.tfnick.getText();

    public MenuBar()  {

        try {
            new SwingEngine(this).render("MenuBar.xml").setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model = new Model();
        listenerMenu();

        tipoUsuario = model.comprobarUsuario(usuarionick).getTipoUsuario();
        nivelAccesoLogin(tipoUsuario);


    }

    public void nivelAccesoLogin(TipoUsuario tipoUsuario) {


        switch (tipoUsuario) {
            case CONSULTA:
                miusuario.setVisible(false);

                break;
        }
    }

    public void listenerMenu(){
        miusuario.addActionListener(this);
        micliente.addActionListener(this);
        miproceso.addActionListener(this);

        cproducto.addActionListener(this);
        cpedido.addActionListener(this);
        cpresupuesto.addActionListener(this);

        infousuarios.addActionListener(this);
        infoclientes.addActionListener(this);
        infoprocesos.addActionListener(this);
        infoproductos.addActionListener(this);
        infopedidos.addActionListener(this);
        infopresupuestos.addActionListener(this);



    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==miusuario){
                new Registro();

        }else if(e.getSource()==micliente){
            new VCliente();

        }else if(e.getSource() == miproceso){
            new vProceso();


        }else if(e.getSource()==cproducto){
            new vProducto();

        }else if(e.getSource()== cpedido){
            new vPedido();

        }else if(e.getSource()==cpresupuesto){
            new vAlbaran();

        }else if(e.getSource()==infousuarios){

            Conexion conexion = new Conexion();
            Connection connection = conexion.getConexion();
            String path = "src\\reportes\\usuarios\\Usuarios.jasper";
            JasperReport reporte = null;
            try {

                reporte = (JasperReport) JRLoader.loadObjectFromFile(String.valueOf(new File(path)));

                JasperPrint print = JasperFillManager.fillReport(path,  null,connection);
                JasperViewer view = new JasperViewer(print,false);

                view.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                view.setVisible(true);

            }catch (JRException eu){
                JOptionPane.showMessageDialog(null , "No se ha podido mostrar el informe");
            }


        }else if(e.getSource()==infoclientes){

            Conexion conexion = new Conexion();
            Connection connection = conexion.getConexion();
            String path = "src\\reportes\\clientes\\Clientes.jasper";
            JasperReport report = null;
            try {


                report = (JasperReport) JRLoader.loadObjectFromFile(String.valueOf(new File(path)));

                JasperPrint print = JasperFillManager.fillReport(path,  null,connection);
                JasperViewer view = new JasperViewer(print,false);

                view.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                view.setVisible(true);
            } catch (JRException e2) {
                JOptionPane.showMessageDialog(null , "No se ha podido mostrar el informe");
            }

        }else if(e.getSource()==infoprocesos){

            Conexion conexion = new Conexion();
            Connection connection = conexion.getConexion();
            String path = "src\\reportes\\procesos\\Procesos.jasper";
            JasperReport report = null;
            try {


                report = (JasperReport) JRLoader.loadObjectFromFile(String.valueOf(new File(path)));

                JasperPrint print = JasperFillManager.fillReport(path,  null,connection);
                JasperViewer view = new JasperViewer(print,false);

                view.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                view.setVisible(true);
            } catch (JRException e1) {
                JOptionPane.showMessageDialog(null , "No se ha podido mostrar el informe");
            }

        }else if(e.getSource()==infoproductos){

            Conexion conexion = new Conexion();
            Connection connection = conexion.getConexion();
            String path = "src\\reportes\\productos\\Productos.jasper";
            JasperReport report = null;
            try {


                report = (JasperReport) JRLoader.loadObjectFromFile(String.valueOf(new File(path)));

                JasperPrint print = JasperFillManager.fillReport(path,  null,connection);
                JasperViewer view = new JasperViewer(print,false);

                view.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                view.setVisible(true);
            } catch (JRException e1) {
               JOptionPane.showMessageDialog(null , "No se ha podido mostrar el informe");
            }

        }else if(e.getSource()==infopedidos){

            Conexion conexion = new Conexion();
            Connection connection = conexion.getConexion();
            String path = "src\\reportes\\pedidos\\Pedidos.jasper";
            JasperReport report = null;
            try{

                report = (JasperReport) JRLoader.loadObjectFromFile(String.valueOf(new File(path)));

                JasperPrint print = JasperFillManager.fillReport(path,  null,connection);
                JasperViewer view = new JasperViewer(print,false);

                view.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                view.setVisible(true);

            }catch(JRException et){
                JOptionPane.showMessageDialog(null , "No se ha podido mostrar el informe");
            }

        }else if(e.getSource() == infopresupuestos){

            Conexion conexion = new Conexion();
            Connection connection = conexion.getConexion();
            String path = "src\\reportes\\presupuestos\\PRESUPUESTOS.jasper";
            JasperReport reporte = null;
            try {

                reporte = (JasperReport) JRLoader.loadObjectFromFile(String.valueOf(new File(path)));

                JasperPrint print = JasperFillManager.fillReport(path,  null,connection);
                JasperViewer view = new JasperViewer(print,false);

                view.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                view.setVisible(true);

            }catch (JRException eu){
                JOptionPane.showMessageDialog(null , "No se ha podido mostrar el informe");
            }
        }

    }
}

