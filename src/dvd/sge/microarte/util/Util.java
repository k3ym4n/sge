package dvd.sge.microarte.util;

import dvd.sge.microarte.face.Botonera;

import javax.swing.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class Util {


    public static String encriptaClaveSH1(String pass){
        String passSH1 = Hash.sha1(pass);
        return passSH1;
    }

    public static boolean esEmail(String correo) {

        // Patrón para validar el email
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

        Matcher mather = pattern.matcher(correo);

        if (mather.find() == false) {

            return false;
        }else{
            return true;
        }

    }

    public static boolean esNumero(String numero){
        Pattern pattern = Pattern.compile("[0-9]+");

        Matcher matcher = pattern.matcher(numero);

        if (matcher.find() == false) {

            return false;
        }else{
            return true;
        }
    }



    public static java.sql.Date formateoDate(Date date){


        java.sql.Date dateSQL = new java.sql.Date(date.getTime());

        return dateSQL;
    }


    public static void loginerror(){
        JOptionPane.showMessageDialog(null,"El nick o la contraseña no son correctos");
    }

    public static void esNumeroError(){
        JOptionPane.showMessageDialog(null,"El telefono debe contener exclusivamente numeros o esta vacio");
    }




}
