package dvd.sge.microarte.util;

public enum EstadoPedido {

    PENDIENTE,PROCESANDO,FINALIZADO
}
