package dvd.sge.microarte.face;

import com.toedter.calendar.JDateChooser;
import dvd.sge.microarte.Controller.ControllerPedido;
import dvd.sge.microarte.Controller.ControllerTopAccion;
import dvd.sge.microarte.Controller.Model;
import dvd.sge.microarte.util.MenuBar;
import dvd.sge.microarte.util.MoverVentanas;
import dvd.sge.microarte.util.Topaccion;

import javax.swing.*;
import java.awt.*;

public class vPedido {
    public JFrame frame;
    public JPanel panelPedido;
    public JTextField tfCodigo;
    public JComboBox cbEstadoPedido;
    public JTextArea taobservaciones;
    public JButton procesandoButton;
    public JTable tablaClientes;
    public JButton btrefrescarTablaClientes;
    public JTextField tfbuscarCliente;
    public JTable tablaPedido;
    public JButton btaddDetallePedido;
    public JButton bteliminarDetallePedido;
    public JTable tablaDetallePedido;
    public JTextField tfreferencia;
    public JTextField tfnombre;
    public JDateChooser Dateinicio;
    public JDateChooser Datefin;
    public JTextField tfimporte;
    public JLabel lbinfo;
    public JLabel lbnick;
    public Botonera botonera;
    public Topaccion topAccion;
    public JTextField tfbuscarCodigo;
    public JTextField tfbuscarReferencia;
    public JDateChooser datebuscarinicio;
    public JDateChooser datebuscarFin;
    public JTextField tfbuscarNombre;
    public JTextField tfbuscarDetalle;
    public JPanel panelMenuBar;
    public JPanel panelBotonera;
    public JPanel panelCentral;
    public JLabel lbcodigoCliente;
    public JLabel lbnombreCliente;
    public JButton btprocesando;
    public JComboBox cbbuscarEstado;
    public JButton btfechas;


    private MoverVentanas mv;
    private ControllerTopAccion accionTop;
    private Model model;
    private ControllerPedido controllerPedido;
    public MenuBar menuBar;



    public vPedido() {
        frame = new JFrame("vPedido");
        frame.setContentPane(panelPedido);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setUndecorated(true);
        frame.pack();
        frame.setVisible(true);

        menuBar = new MenuBar();
        panelMenuBar.add(menuBar.panelPrincipal);
        mv = new MoverVentanas(frame);
        menuBar.panelPrincipal.setBackground(Color.decode("#276989"));

        model = new Model();
        accionTop = new ControllerTopAccion(this,this.getTopAccion());
        controllerPedido = new ControllerPedido(this,model,this.getBotonera());



    }

    public Botonera getBotonera() {
        return botonera;
    }

    public void setBotonera(Botonera botonera) {
        this.botonera = botonera;
    }

    public Topaccion getTopAccion() {
        return topAccion;
    }

    public void setTopAccion(Topaccion topAccion) {
        this.topAccion = topAccion;
    }
}
