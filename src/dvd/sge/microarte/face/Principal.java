package dvd.sge.microarte.face;

import dvd.sge.microarte.Controller.ControllerPrincipal;
import dvd.sge.microarte.Controller.ControllerTopAccion;
import dvd.sge.microarte.Controller.Model;
import dvd.sge.microarte.util.MoverVentanas;
import dvd.sge.microarte.util.Topaccion;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Principal {
    public JFrame frame;

    public JLabel lbaddUsuario;
    public JLabel lbaddCliente;
    public JLabel lbaddPedido;
    public JLabel lbaddProducto;
    public JLabel lbaddProceso;
    public JLabel lbaddFactura;
    public JPanel panelObj;
    private Topaccion accionTop;


    private MoverVentanas mv;
    private ControllerTopAccion controllerTopAccion;
    private ControllerPrincipal controllerPrincipal;
    private Model model;


    public Registro vRegistro;





    public Principal() {
        frame = new JFrame("Principal");
        frame.setContentPane(panelObj);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setUndecorated(true);//elimina el borde del Jframe
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);


        frame.setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/img/rabbitrun.png")));
        mv = new MoverVentanas(frame);
        controllerTopAccion = new ControllerTopAccion(this,this.getAccionTop(),this.getvRegistro());

        model = new Model();
        controllerPrincipal = new ControllerPrincipal(this,model);

    }

    public Topaccion getAccionTop() {
        return accionTop;
    }

    public void setAccionTop(Topaccion accionTop) {
        this.accionTop = accionTop;
    }

    public Registro getvRegistro() {
        return vRegistro;
    }

    public void setvRegistro(Registro vRegistro) {
        this.vRegistro = vRegistro;
    }
}
