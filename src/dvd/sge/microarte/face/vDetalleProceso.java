package dvd.sge.microarte.face;

import dvd.sge.microarte.Controller.ControllerTopAccion;
import dvd.sge.microarte.Controller.Model;
import dvd.sge.microarte.base.Proceso;
import dvd.sge.microarte.base.Producto;
import dvd.sge.microarte.util.Topaccion;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.util.List;

public class vDetalleProceso extends JDialog implements ActionListener {
    private JPanel PanelDetalleProceso;
    private JPanel PanelTop;
    private JPanel panelCentral;
    private Topaccion accionTop;
    public JTextField tfbuscarProceso;
    public JTable tablaProcesos;
    public JButton btcancelarProceso;
    public JButton btanandirProceso;
    public JTextField tftiempo;
    public JLabel lbProducto;
    private JLabel lbinfo;


    private ControllerTopAccion controllerTopAccion;
    private  Model model;

    private DefaultTableModel dtmProceso;

    private Producto producto;
    private Proceso proceso;
    private float tiempo;


    public vDetalleProceso() {

        setContentPane(PanelDetalleProceso);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setUndecorated(true);
        setModal(true);
        pack();

        controllerTopAccion = new ControllerTopAccion(this,this.getAccionTop());
        model = new Model();

        listenerBotones();
        modeloTablaProceso();
        listarTablaProcesos();
        buscarProceso();




    }


    private void listenerBotones(){
        btanandirProceso.addActionListener(this);
        btcancelarProceso.addActionListener(this);
    }



    private void limpiarCampos(){
        tfbuscarProceso.setText("");
        tftiempo.setText("");
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource() == btanandirProceso){

            if(tftiempo.getText().isEmpty()){
                JOptionPane.showMessageDialog(null,"El campo tiempo esta vacio");
                return ;
            }else{
                tiempo = Float.parseFloat(tftiempo.getText());
                try{
                    proceso = listenerTablaProcesos();
                }catch(ArrayIndexOutOfBoundsException aiofbe){
                    JOptionPane.showMessageDialog(null,"Selecciona un Proceso por favor (doble click)");
                    return;
                }

                producto = model.getProducto(lbProducto.getText());
                limpiarCampos();

            }
            setVisible(false);

        }else if(e.getSource()== btcancelarProceso){
            limpiarCampos();
            setVisible(false);
        }

    }

    private Proceso listenerTablaProcesos(){

        String codigo = (String) tablaProcesos.getValueAt(tablaProcesos.getSelectedRow(),0);
        Proceso proceso = model.getProceso(codigo);

        return proceso;
    }

    private void listarTablaProcesos(){
        List<Proceso> listaProceso = model.getListaProcesos();
        dtmProceso.setRowCount(0);
        for(Proceso proceso : listaProceso){
            Object [] filaProceso = new Object[]{proceso.getCodigo(),proceso.getNombre(),proceso.getPvp_minimo(),proceso.getPvp_hora()};
            dtmProceso.addRow(filaProceso);
        }
    }

    public void visible(){
        setVisible(true);
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Proceso getProceso() {
        return proceso;
    }

    public void setProceso(Proceso proceso) {
        this.proceso = proceso;
    }

    public Topaccion getAccionTop() {
        return accionTop;
    }

    public void setAccionTop(Topaccion accionTop) {
        this.accionTop = accionTop;
    }

    public float getTiempo() {
        return tiempo;
    }

    public void setTiempo(float tiempo) {
        this.tiempo = tiempo;
    }


    private void buscarProceso(){

        tfbuscarProceso.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                tfbuscarProceso.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                tfbuscarProceso.setText("Proceso...");
                listarTablaProcesos();
            }
        });

        tfbuscarProceso.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = tfbuscarProceso.getText();
                System.out.println(texto);
                List<Proceso> listaProceso = model.buscarProcesoNombre(texto);
                dtmProceso.setRowCount(0);
                for(Proceso proceso : listaProceso){
                    Object [] filaProceso = new Object[]{proceso.getCodigo(),proceso.getNombre(),proceso.getPvp_minimo(),proceso.getPvp_hora()};
                    dtmProceso.addRow(filaProceso);
                }
            }
        });
    }

    private void modeloTablaProceso(){
        dtmProceso = new DefaultTableModel();
        dtmProceso.addColumn("Codigo");
        dtmProceso.addColumn("Nombre");
        dtmProceso.addColumn("pvp/Minimo");
        dtmProceso.addColumn("pvp/Hora");
        tablaProcesos.setModel(dtmProceso);
    }
}
