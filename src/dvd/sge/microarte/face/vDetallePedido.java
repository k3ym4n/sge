package dvd.sge.microarte.face;

import dvd.sge.microarte.Controller.ControllerTopAccion;
import dvd.sge.microarte.Controller.Model;
import dvd.sge.microarte.base.Pedido;
import dvd.sge.microarte.base.Producto;
import dvd.sge.microarte.util.MoverVentanas;
import dvd.sge.microarte.util.Topaccion;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.util.List;

public class vDetallePedido extends JDialog implements ActionListener {

    private JPanel panelDetallePedido;
    private JPanel panelTop;
    private JPanel PanelCentral;
    public JTextField tfbuscarProducto;
    public JTextField tfunidades;
    public JButton btcancelar;
    public JButton btanadir;
    public JTable tablaDetallesProducto;
    public Topaccion accionTop;
    public JLabel lbcodigoPedido;
    private JLabel lbinfo;


    private ControllerTopAccion controllerTopAccion;

    public Model model;

    private DefaultTableModel dtmDetalle;
    private int cantidad;
    private Producto producto;
    private Pedido pedido;

    public vDetallePedido() {

        setContentPane(panelDetallePedido);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setUndecorated(true);
        setModal(true);
        pack();

        controllerTopAccion = new ControllerTopAccion(this,this.getAccionTop());
        model = new Model();

        listenerBotones();
        modeloTablaDetalle();
        listarTablaProductos();

        buscarProducto();



    }

    private void listenerBotones(){
        btanadir.addActionListener(this);
        btcancelar.addActionListener(this);
    }

    private void limpiarCampos(){
        tfbuscarProducto.setText("");
        tfunidades.setText("");
    }

    private void modeloTablaDetalle(){
        dtmDetalle = new DefaultTableModel();
        dtmDetalle.addColumn("Codigo");
        dtmDetalle.addColumn("Nombre");
        dtmDetalle.addColumn("Observaciones");
        dtmDetalle.addColumn("Precio");
        tablaDetallesProducto.setModel(dtmDetalle);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource() == btanadir){

            if(tfunidades.getText().isEmpty()){
                JOptionPane.showMessageDialog(null, "Indica la cantidad",
                        "Seleccionar Producto", JOptionPane.ERROR_MESSAGE);
                return;
            }else{

                cantidad = Integer.parseInt(tfunidades.getText());
                try{
                    producto = listenerTablaProductos();
                }catch(ArrayIndexOutOfBoundsException aiofbe){
                    JOptionPane.showMessageDialog(null,"Selecciona un Producto por favor (doble click)");
                    return;
                }

                pedido = model.getPedido(lbcodigoPedido.getText());
                limpiarCampos();


            }
            setVisible(false);

        }else if(e.getSource() == btcancelar){
            limpiarCampos();
            setVisible(false);
        }

    }

    private void listarTablaProductos(){
        List<Producto> listaProducto = model.getListaProductos();
        dtmDetalle.setRowCount(0);
        for(Producto producto : listaProducto){
            Object [] filaDetalle = new Object[]{producto.getCodigo(),producto.getDenominacion(),producto.getObservaciones(),producto.getImporte()};
            dtmDetalle.addRow(filaDetalle);

        }

    }

    private Producto listenerTablaProductos(){

        String codigo = (String) tablaDetallesProducto.getValueAt(tablaDetallesProducto.getSelectedRow(),0);

        Producto producto = model.getProducto(codigo);


        return producto;

    }

    public Topaccion getAccionTop() {
        return accionTop;
    }

    public void setAccionTop(Topaccion accionTop) {
        this.accionTop = accionTop;
    }

    public void visible(){
        setVisible(true);
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    private void buscarProducto(){

        tfbuscarProducto.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                tfbuscarProducto.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                tfbuscarProducto.setText("Producto...");
                listarTablaProductos();
            }
        });

        tfbuscarProducto.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = tfbuscarProducto.getText();
                List<Producto> listaProducto = model.buscarProductoDenominacion(texto);
                dtmDetalle.setRowCount(0);
                for(Producto producto : listaProducto){
                    Object [] filaDetalle = new Object[]{producto.getCodigo(),producto.getDenominacion(),producto.getObservaciones(),producto.getImporte()};
                    dtmDetalle.addRow(filaDetalle);

                }
            }
        });
    }
}
