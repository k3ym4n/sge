package dvd.sge.microarte.face;

import dvd.sge.microarte.Controller.ControllerCliente;
import dvd.sge.microarte.Controller.ControllerTopAccion;
import dvd.sge.microarte.Controller.Model;
import dvd.sge.microarte.util.MenuBar;
import dvd.sge.microarte.util.MoverVentanas;
import dvd.sge.microarte.util.Topaccion;

import javax.swing.*;
import java.awt.*;


public class VCliente {
    public JFrame frame;
    public JPanel panelCliente;
    public JPanel panelBotonera;
    private JPanel panelMenu;
    public JTextField tfnombre;
    public JComboBox cbtipoCliente;
    public JTable tablaClientes;
    public JPanel panelCentral;
    public JTextField tfdireccion;
    public JTextField tfCP;
    public JTextField tfprovincia;
    public JTextField tfemail;
    public JTextField tfdni;
    public JTextField tftelefono1;
    public JTextField tftelefono2;
    public JTextField tfcif;
    public JTextField tfcodigo;
    public JLabel lbinfo;
    public JLabel lbnick;
    public Botonera botonera;
    public JScrollPane scrollCliente;
    private MoverVentanas mv;
    private ControllerTopAccion accionTop;
    public Topaccion topaccion;
    public JTextField tfbuscarNombre;
    public JTextField tfbuscarEmail;
    public JTextField tfbuscarCodigo;
    public JComboBox cbbuscarTipo;
    public JButton btinformeClientes;
    public MenuBar menuBar;




    private Model model;
    private ControllerCliente controllerCliente;

    public VCliente() {
        frame = new JFrame("VCliente");
        frame.setContentPane(panelCliente);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setUndecorated(true);
        frame.pack();
        frame.setVisible(true);
        tablaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        frame.setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/img/rabbitrun.png")));

        menuBar = new MenuBar();
        panelMenu.add(menuBar.panelPrincipal);
        menuBar.panelPrincipal.setBackground(Color.decode("#276989"));
        mv = new MoverVentanas(frame);
        accionTop = new ControllerTopAccion(this,this.getTopaccion());
        model = new Model();
        controllerCliente = new ControllerCliente(this,model,this.getBotonera());


    }

    public Botonera getBotonera() {
        return botonera;
    }

    public void setBotonera(Botonera botonera) {
        this.botonera = botonera;
    }

    public Topaccion getTopaccion() {
        return topaccion;
    }

    public void setTopaccion(Topaccion topaccion) {
        this.topaccion = topaccion;
    }
}
