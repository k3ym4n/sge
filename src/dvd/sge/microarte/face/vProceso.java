package dvd.sge.microarte.face;

import dvd.sge.microarte.Controller.ControllerProceso;
import dvd.sge.microarte.Controller.ControllerTopAccion;
import dvd.sge.microarte.Controller.Model;
import dvd.sge.microarte.util.MenuBar;
import dvd.sge.microarte.util.MoverVentanas;
import dvd.sge.microarte.util.Topaccion;

import javax.swing.*;
import java.awt.*;

public class vProceso {
    public JFrame frame;
    private JPanel PanelProceso;
    public JPanel PanelBotonera;
    public JPanel panelMenu;
    private JPanel PanelCentro;
    public JTextPane tpObservaciones;
    public JTextField tfcodigo;
    public JTable tablaProceso;
    public JTextField tfnombre;
    public JTextField tfpvpminimo;
    public JTextField tfpvphora;
    private Topaccion acciontop;
    private JScrollPane scrollProceso;
    private Botonera Botonera;
    public JLabel lbinfo;
    public JLabel lbnick;
    public JTextField tfbuscarcodigo;
    public JTextField tfbuscarnombre;
    public JTextField tfbuscarpvpminimo;
    public JTextField tfbuscarpvphora;

    private MoverVentanas mv;
    private ControllerTopAccion controllerTopAccion;
    private Model model;
    private ControllerProceso controllerProceso;
    public MenuBar menuBar;


    public vProceso() {
        frame = new JFrame("vProceso");
        frame.setContentPane(PanelProceso);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setUndecorated(true);
        frame.pack();
        frame.setVisible(true);

        menuBar = new MenuBar();
        panelMenu.add(menuBar.panelPrincipal);
        menuBar.panelPrincipal.setBackground(Color.decode("#276989"));

        tablaProceso.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

        mv = new MoverVentanas(frame);
        controllerTopAccion = new ControllerTopAccion(this,this.getAcciontop());
        model = new Model();
        controllerProceso = new ControllerProceso(this,model,this.getBotonera());

        frame.setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/img/rabbitrun.png")));



    }

    public Topaccion getAcciontop() {
        return acciontop;
    }

    public void setAcciontop(Topaccion acciontop) {
        this.acciontop = acciontop;
    }

    public dvd.sge.microarte.face.Botonera getBotonera() {
        return Botonera;
    }

    public void setBotonera(dvd.sge.microarte.face.Botonera botonera) {
        Botonera = botonera;
    }
}
