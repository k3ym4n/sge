package dvd.sge.microarte.face;

import dvd.sge.microarte.Controller.ControllerRegistro;
import dvd.sge.microarte.Controller.ControllerTopAccion;
import dvd.sge.microarte.Controller.Model;
import dvd.sge.microarte.util.MoverVentanas;
import dvd.sge.microarte.util.Topaccion;



import javax.swing.*;
import java.awt.*;

public class Registro {
    public JFrame frame;
    private JPanel panelTodoRegistro;
    public JPanel panelBotonera;
    public JPanel panelRegistro;
    public JComboBox cbTipousuario;
    public JTextField tfnombre;
    public JTable tablausuarios;
    public Botonera Botonera;
    public JTextField tfapellidos;
    public JTextField tfdireccion;
    public JTextField tfemail;
    public JTextField tftelefono1;
    public JTextField tftelefono2;
    public JTextField tfnick;
    public JPasswordField pfcontrasena;
    public JLabel lbinfo;
    public JLabel lbnick;
    public JPasswordField pfconfirmcontrasena;

    private MoverVentanas mv;
    private ControllerTopAccion accionTop;
    private Topaccion topaccion;
    private JScrollPane scrollregistro;
    public JButton btinforme;

    private ControllerRegistro controllerRegistro;
    private Model model;


    public Registro() {
        frame = new JFrame("Registro");
        frame.setContentPane(panelTodoRegistro);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setUndecorated(true);
        frame.pack();
        frame.setVisible(true);

        tablausuarios.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);




        frame.setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/img/rabbitrun.png")));

        mv = new MoverVentanas(frame);
        accionTop = new ControllerTopAccion(this,this.getTopaccion());
        model = new Model();
        controllerRegistro = new ControllerRegistro(this,model,this.getBotonera());
    }

    public Botonera getBotonera() {
        return Botonera;
    }

    public void setBotonera(dvd.sge.microarte.face.Botonera botonera) {
        Botonera = botonera;
    }

    public Topaccion getTopaccion() {
        return topaccion;
    }

    public void setTopaccion(Topaccion topaccion) {
        this.topaccion = topaccion;
    }
}
