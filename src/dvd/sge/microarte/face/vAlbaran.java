package dvd.sge.microarte.face;

import com.toedter.calendar.JDateChooser;
import dvd.sge.microarte.Controller.ControllerAlbaran;
import dvd.sge.microarte.Controller.ControllerTopAccion;
import dvd.sge.microarte.Controller.Model;
import dvd.sge.microarte.util.MenuBar;
import dvd.sge.microarte.util.MoverVentanas;
import dvd.sge.microarte.util.Topaccion;

import javax.swing.*;
import java.awt.*;

public class vAlbaran {
    public JFrame frame;
    private JPanel panleAlbaran;
    public JPanel panelBotonera;
    public JPanel panelTop;
    private JPanel panelCentral;
    public JTextField tfnumeroAlbaran;
    public JTextPane tpobservaciones;
    public JTable tablaPresupuestos;
    public JTable tablaPedidos;
    public JTextField tfnumeroPresupuesto;
    public JTextField tfimporte;
    public Botonera botonera;
    public Topaccion topaccion;
    public JLabel lbinfo;
    public JLabel lbNombreCliente;
    public JLabel lbcodigoPedido;
    public JLabel lbNombrePedido;
    public JLabel lbimportePedido;
    public JComboBox cbestadoPresupuesto;
    public JLabel lbnumeroPresupuesto;
    public JLabel lbnick;
    public JTextField tfbuscarPedido;
    public JTextField tfbuscarPresupuesto;
    public JDateChooser fechaInicio;
    public JDateChooser fechaFin;
    public JButton btbuscarFechas;
    public JButton btrefrescar;
    public MenuBar menuBar;


    private MoverVentanas mv;
    private ControllerTopAccion controllerTopAccion;
    private Model model;
    private ControllerAlbaran controllerAlbaran;


    public vAlbaran() {
        frame = new JFrame("vAlbaran");
        frame.setContentPane(panleAlbaran);
        frame.setUndecorated(true);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        menuBar = new MenuBar();
        panelTop.add(menuBar.panelPrincipal);
        menuBar.panelPrincipal.setBackground(Color.decode("#276989"));
        mv = new MoverVentanas(frame);
        controllerTopAccion = new ControllerTopAccion(this,this.getTopaccion());
        model = new Model();
        controllerAlbaran = new ControllerAlbaran(this,model,this.getBotonera());


    }

    public Botonera getBotonera() {
        return botonera;
    }

    public void setBotonera(Botonera botonera) {
        this.botonera = botonera;
    }

    public Topaccion getTopaccion() {
        return topaccion;
    }

    public void setTopaccion(Topaccion topaccion) {
        this.topaccion = topaccion;
    }
}
