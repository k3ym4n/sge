package dvd.sge.microarte.face;

import dvd.sge.microarte.Controller.ControllerProducto;
import dvd.sge.microarte.Controller.ControllerTopAccion;
import dvd.sge.microarte.Controller.Model;
import dvd.sge.microarte.util.MenuBar;
import dvd.sge.microarte.util.MoverVentanas;
import dvd.sge.microarte.util.Topaccion;

import javax.swing.*;
import java.awt.*;

public class vProducto {
    public JFrame frame;
    private JPanel PanelProducto;
    public JPanel PanelMenu;
    public JPanel PanelBotonera;
    private JPanel PanelCentro;
    public JTextPane tpobservaciones;
    public JComboBox cbtintas;
    public JTable tablaProducto;
    public JButton btDetalleProceso;
    public JLabel lbinfo;
    public JLabel lbnick;
    public Topaccion accionTop;
    public Botonera botonera;
    public JTextField tfcodigo;
    public JTextField tfreferencia;
    public JTextField tfdenominacion;
    public JTextField tfimporte;
    public JButton bteliminarproceso;
    public JTable tablaProcesos;
    public JTextField tfbuscarcodigo;
    public JTextField tfbuscarreferencia;
    public JTextField tfbuscardenominacion;
    public JTextField tfimportemin;

    public MenuBar menuBar;

    private MoverVentanas mv;
    private ControllerTopAccion controllerTopAccion;
    private Model model;
    private ControllerProducto controllerProducto;


    public vProducto() {
        frame = new JFrame("vProducto");
        frame.setContentPane(PanelProducto);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setUndecorated(true);
        frame.pack();
        frame.setVisible(true);


        frame.setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/img/rabbitrun.png")));

        menuBar = new MenuBar();
        PanelMenu.add(menuBar.panelPrincipal);
        menuBar.panelPrincipal.setBackground(Color.decode("#276989"));

        mv = new MoverVentanas(frame);
        controllerTopAccion = new ControllerTopAccion(this,this.getAccionTop());
        model = new Model();
        controllerProducto = new ControllerProducto(this,model,this.getBotonera());

    }

    public Topaccion getAccionTop() {
        return accionTop;
    }

    public void setAccionTop(Topaccion accionTop) {
        this.accionTop = accionTop;
    }

    public Botonera getBotonera() {
        return botonera;
    }

    public void setBotonera(Botonera botonera) {
        this.botonera = botonera;
    }
}
