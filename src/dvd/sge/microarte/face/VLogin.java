package dvd.sge.microarte.face;


import dvd.sge.microarte.Controller.ControllerLogin;
import dvd.sge.microarte.Controller.ControllerRegistro;
import dvd.sge.microarte.Controller.ControllerTopAccion;
import dvd.sge.microarte.Controller.Model;
import dvd.sge.microarte.util.MoverVentanas;
import dvd.sge.microarte.util.Topaccion;

import javax.swing.*;
import java.awt.*;

public class VLogin {

    public JFrame frame;
    public JPanel panelObj;
    public JLabel lbiconusuario;
    public JTextField tfnick;
    public JPasswordField pfcontrasena;
    public JButton btacceder;
    public JButton btcancelar;
    private MoverVentanas mv;
    private ControllerTopAccion accionTop;
    private Topaccion topaccion;
    private ControllerLogin controllerLogin;
    private Model model;


    public VLogin() {
        frame = new JFrame("Login");
        frame.setContentPane(panelObj);
        frame.setUndecorated(true);//elimina el borde del Jframe
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setSize(200, 200);
        frame.pack();
        frame.setVisible(true);
        frame.setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/img/rabbitrun.png")));

        mv = new MoverVentanas(frame);

        accionTop = new ControllerTopAccion(this,this.getTopaccion());


        model = new Model();
        controllerLogin = new ControllerLogin(this,model);


    }

    public Topaccion getTopaccion() {
        return topaccion;
    }

    public void setTopaccion(Topaccion topaccion) {
        this.topaccion = topaccion;
    }
}




