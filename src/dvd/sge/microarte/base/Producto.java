package dvd.sge.microarte.base;

import dvd.sge.microarte.util.Tintas;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "productos")
public class Producto implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "codigo")
    private String codigo;
    @Column(name = "referencia")
    private String referencia;
    @Column(name = "denominacion")
    private String denominacion;
    @Enumerated(EnumType.STRING)
    @Column(name = "tintas")
    private Tintas tintas;
    @Column(name = "importe")
    private float importe;
    @Column(name = "observaciones")
    private String observaciones;

    @OneToMany(mappedBy = "producto")
    private List<DetallePedido> detallePedidos;



    public Producto(){

        detallePedidos = new ArrayList<>();


    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDenominacion() {
        return denominacion;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }

    public Tintas getTintas() {
        return tintas;
    }

    public void setTintas(Tintas tintas) {
        this.tintas = tintas;
    }

    public float getImporte() {
        return importe;
    }

    public void setImporte(float importe) {
        this.importe = importe;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public List<DetallePedido> getDetallePedidos() {
        return detallePedidos;
    }

    public void setDetallePedidos(List<DetallePedido> detallePedidos) {
        this.detallePedidos = detallePedidos;
    }


}
