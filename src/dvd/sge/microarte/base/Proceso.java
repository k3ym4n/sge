package dvd.sge.microarte.base;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "procesos")
public class Proceso implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private int id;
    @Column(name = "codigo")
    private String codigo;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "pvp_minimo")
    private float pvp_minimo;
    @Column(name = "pvp_hora")
    private float pvp_hora;
    @Column(name = "observaciones")
    private String observaciones;

    @OneToMany(mappedBy = "proceso")
    private List<DetalleProceso> detalleProcesos;



    public Proceso(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPvp_minimo() {
        return pvp_minimo;
    }

    public void setPvp_minimo(float pvp_minimo) {
        this.pvp_minimo = pvp_minimo;
    }

    public float getPvp_hora() {
        return pvp_hora;
    }

    public void setPvp_hora(float pvp_hora) {
        this.pvp_hora = pvp_hora;
    }


    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public List<DetalleProceso> getDetalleProcesos() {
        return detalleProcesos;
    }

    public void setDetalleProcesos(List<DetalleProceso> detalleProcesos) {
        this.detalleProcesos = detalleProcesos;
    }


}
