package dvd.sge.microarte.base;




import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "presupuestos")
public class Albaran implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "fecha")
    private Date fecha;
    @Column(name = "importe")
    private float importe;
    @Column(name = "numpresupuesto")
    private int numpresupuesto;
    @Column(name = "observaciones")
    private String observaciones;

    @OneToOne
    @JoinColumn(name = "id_pedido")
    private Pedido pedido;



    public Albaran(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public float getImporte() {
        return importe;
    }

    public void setImporte(float importe) {
        this.importe = importe;
    }



    public int getNumpresupuesto() {
        return numpresupuesto;
    }

    public void setNumpresupuesto(int numpresupuesto) {
        this.numpresupuesto = numpresupuesto;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }


}
