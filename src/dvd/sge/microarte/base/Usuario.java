package dvd.sge.microarte.base;

import dvd.sge.microarte.util.TipoUsuario;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "usuarios")
public class Usuario implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellido")
    private String apellido;
    @Column(name = "direccion")
    private String direccion;
    @Email
    @Column(name = "email")
    private String email;
    @Column(name = "fecha_registro")
    private Date fecha_registro;
    @Column(name = "telefono_fijo")
    private String tlf_fijo;
    @Column(name = "telefono_movil")
    private String tlf_movil;
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo")
    private TipoUsuario tipoUsuario;
    @Column(name = "nick" , unique = true)
    private String nick;
    @Column(name = "password")
    private String password;


    @OneToMany(mappedBy = "usuario")
    private List<Cliente> clientes;


    public Usuario(){
        clientes = new ArrayList<>();

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public String getTlf_fijo() {
        return tlf_fijo;
    }

    public void setTlf_fijo(String tlf_fijo) {
        this.tlf_fijo = tlf_fijo;
    }

    public String getTlf_movil() {
        return tlf_movil;
    }

    public void setTlf_movil(String tlf_movil) {
        this.tlf_movil = tlf_movil;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }
}
